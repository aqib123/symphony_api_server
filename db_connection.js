var Sequelize = require('sequelize');
var sequelize = new Sequelize(process.env.DB_NEW_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: 'localhost',
    port: process.env.DB_PORT,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});
sequelize.authenticate().then(() => {
    console.log('Database Connection has been established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database:', err);
});
// module.exports = DB;
// module.exports = dbMysql;