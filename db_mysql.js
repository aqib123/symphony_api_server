var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    multipleStatements: true,
    debug: false
});
// var dbMysql = function() {
//     var self = this;
//     self.executeQuery = function(info, query, param, callback) {
//         pool.getConnection(function(err, connection) {
//             if (err) {
//                 console.log("Error in connection database");
//                 if (connection) connection.release();
//                 throw err;
//             }
//             console.log(info + ': ' + connection.threadId);
//             connection.query(query, param, function(err, rows) {
//                 connection.release();
//                 console.log(rows);
//                 if (!err) {
//                     callback(rows);
//                 } else console.log(err);
//             });
//             connection.on('error', function(err) {
//                 console.log("Error in connection database");
//                 throw err;
//                 return;
//             });
//         });
//     }
// };
var DB = function() {
    function _query(query, params, callback) {
        pool.getConnection(function(err, connection) {
            if (err) {
                connection.release();
                callback(null, err);
                throw err;
            }
            connection.query(query, params, function(err, rows) {
                connection.release();
                if (!err) {
                    callback(rows);
                } else {
                    callback(null, err);
                }
            });
            connection.on('error', function(err) {
                connection.release();
                callback(null, err);
                throw err;
            });
        });
    };
    return {
        query: _query
    };
};
module.exports = DB;
// module.exports = dbMysql;