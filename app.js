require('dotenv').config();
var express = require('express');
var app = express();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var cors = require('cors'); //after the line var bodyParser = require('body-parser');
//var passport = require('passport')
var session = require('express-session');
//var check = require('validator').check;
const {
    check,
    validationResult
} = require('express-validator/check');
const {
    matchedData
} = require('express-validator/filter');
var server = require('http').Server(app);
var expressValidator = require('express-validator');
var db = new(require('./db_mysql.js'));
// var db_connection = require('./db_connection.js');
var request = require('request');
var path = require('path');
//var dx = new (require('./dx.js'))(db, MQ);
var jwt = require('jwt-simple')
//var users = require('./user');
// var routes = require('./routes/api.routes');
server.listen(process.env.GAME_PORT, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
//console.log(path.join(__dirname + '/views/images'));
app.use(express.static(path.join(__dirname + '/layouts')));
app.use(cookieParser());
app.use(bodyParser.json({
    extended: true
})); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: false
}));
// support encoded bodies
//app.use(cors()); //after the line app.use(logger('dev'));
app.use(expressValidator());
app.use(expressValidator({
    customValidators: {
        isArray: function(value) {
            console.log(value)
            console.log(Array.isArray(value))
            return Array.isArray(value);
        }
    }
}));
app.use(session({
    secret: 'mySecret!'
}));
var sess;
app.use(function(req, res, next) {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
        var decoded = jwt.decode(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET);
        if (decoded) {
            req.user = decoded
            next();
        } else {
            req.user = undefined;
            next();
        }
    } else {
        req.user = undefined;
        next();
    }
});
const appRoutes = require('./api/routes.js')(app);