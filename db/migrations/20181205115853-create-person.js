'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('people', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            gender: {
                allowNull: false,
                type: Sequelize.TINYINT
            },
            dob: {
                allowNull: true,
                type: Sequelize.INTEGER
            },
            can_login: {
                allowNull: false,
                type: Sequelize.TINYINT
            },
            address: {
                allowNull: true,
                type: Sequelize.INTEGER
            },
            first_name: {
                allowNull: false,
                type: Sequelize.STRING(100)
            },
            last_name: {
                allowNull: false,
                type: Sequelize.STRING(100)
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            deletedAt: {
                allowNull: true,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('people');
    }
};