'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('courses', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            course_template: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            commencing_on: {
                allowNull: false,
                type: Sequelize.DATE
            },
            reference_text: {
                type: Sequelize.STRING
            },
            studio: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            facility: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            schedule: {
                allowNull: false,
                type: Sequelize.STRING
            },
            max_student: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            min_student: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            required_confirmation: {
                allowNull: false,
                type: Sequelize.TINYINT
            },
            tutor: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            course_fee_type: {
                allowNull: true,
                type: Sequelize.STRING
            },
            expiry_date: {
                allowNull: false,
                type: Sequelize.INTEGER
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('courses');
    }
};