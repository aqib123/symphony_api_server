'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('classes', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            course: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            time: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            tutor: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            studio: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('classes');
    }
};