'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('addresses', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            line_1: {
                allowNull: true,
                type: Sequelize.STRING
            },
            line_2: {
                allowNull: true,
                type: Sequelize.STRING
            },
            city: {
                allowNull: true,
                type: Sequelize.STRING
            },
            postcode: {
                allowNull: true,
                type: Sequelize.STRING
            },
            province: {
                allowNull: true,
                type: Sequelize.STRING
            },
            country: {
                allowNull: false,
                type: Sequelize.STRING
            },
            latitude: {
                allowNull: true,
                type: Sequelize.DECIMAL
            },
            longitude: {
                allowNull: true,
                type: Sequelize.DECIMAL
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('addresses');
    }
};