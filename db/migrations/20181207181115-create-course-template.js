'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('course_templates', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING(100)
            },
            description: {
                type: Sequelize.STRING(800)
            },
            payment_mode: {
                allowNull: false,
                type: Sequelize.TINYINT
            },
            group_individual: {
                allowNull: false,
                type: Sequelize.TINYINT
            },
            studio: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            currency: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            price: {
                allowNull: false,
                type: Sequelize.DECIMAL
            },
            category: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            sub_category: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            number_of_class: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            class_duration: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            course_image: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            course_requirement: {
                allowNull: true,
                type: Sequelize.STRING
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('course_templates');
    }
};