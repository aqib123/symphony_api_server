'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('transactions', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            time: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            madeby: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            description: {
                type: Sequelize.STRING
            },
            currency: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            amount: {
                allowNull: false,
                type: Sequelize.DECIMAL
            },
            course: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            student: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('transactions');
    }
};