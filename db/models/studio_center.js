'use strict';
module.exports = (sequelize, DataTypes) => {
    const studio_center = sequelize.define('studio_center', {
        name: DataTypes.STRING,
        studio: DataTypes.INTEGER,
        address: DataTypes.INTEGER
    }, {});
    studio_center.associate = function(models) {
        // associations can be defined here
        studio_center.belongsTo(models.studio, {
            foreignKey: 'studio',
            as: 'studioInformation'
        })
        studio_center.belongsTo(models.address, {
            foreignKey: 'address',
            as: 'addressInformation'
        })
        studio_center.hasMany(models.facility, {
            foreignKey: 'studio_center',
            as: 'facilitiesInformation'
        })
    };
    return studio_center;
};