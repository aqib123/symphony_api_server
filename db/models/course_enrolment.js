'use strict';
module.exports = (sequelize, DataTypes) => {
    const course_enrolment = sequelize.define('course_enrolment', {
        course_id: DataTypes.INTEGER,
        student: DataTypes.INTEGER,
        paid_at: DataTypes.INTEGER,
        paid_amount: DataTypes.DECIMAL
    }, {});
    course_enrolment.associate = function(models) {
        // associations can be defined here
        course_enrolment.belongsTo(models.course, {
            foreignKey: 'course_id',
            as: 'courseDetails'
        })
        course_enrolment.belongsTo(models.person, {
            foreignKey: 'student',
            as: 'StudentEnrolledRecord',
        })
    };
    return course_enrolment;
};