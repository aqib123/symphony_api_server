'use strict';
module.exports = (sequelize, DataTypes) => {
  const blak_list_tokens = sequelize.define('blak_list_tokens', {
    token: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {});
  blak_list_tokens.associate = function(models) {
    // associations can be defined here
  };
  return blak_list_tokens;
};