'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
    const login = sequelize.define('login', {
        password: DataTypes.STRING,
        mobile_number: DataTypes.STRING,
        email: DataTypes.STRING,
        mobile_number_country_iso: DataTypes.STRING,
        person: DataTypes.INTEGER
    }, {
        instanceMethods: {
            generateHash(password) {
                return bcrypt.hash(password, bcrypt.genSaltSync(8));
            },
            validPassword(password) {
                return bcrypt.compare(password, this.password);
            }
        }
    });
    login.associate = function(models) {
        // associations can be defined here
        login.belongsTo(models.person, {
            foreignKey: 'person',
            as: 'personRecord',
        })
    };
    return login;
};