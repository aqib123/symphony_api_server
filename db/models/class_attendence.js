'use strict';
module.exports = (sequelize, DataTypes) => {
  const class_attendence = sequelize.define('class_attendence', {
    class: DataTypes.INTEGER,
    student: DataTypes.INTEGER,
    time: DataTypes.INTEGER
  }, {});
  class_attendence.associate = function(models) {
    // associations can be defined here
  };
  return class_attendence;
};