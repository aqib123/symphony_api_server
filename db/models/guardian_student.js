'use strict';
module.exports = (sequelize, DataTypes) => {
    const guardian_student = sequelize.define('guardian_student', {
        guardian: DataTypes.INTEGER,
        student: DataTypes.INTEGER,
        is_owner: DataTypes.TINYINT,
        deletedAt: DataTypes.DATE
    }, {});
    guardian_student.associate = function(models) {
        // associations can be defined here
        guardian_student.belongsTo(models.person, {
            foreignKey: 'student',
            as: 'studentPersonalRecord'
        })
    };
    return guardian_student;
};