'use strict';
module.exports = (sequelize, DataTypes) => {
    const currency = sequelize.define('currency', {
        name: DataTypes.STRING,
        code: DataTypes.STRING
    }, {});
    currency.associate = function(models) {
        // associations can be defined here
    };
    return currency;
};