'use strict';
module.exports = (sequelize, DataTypes) => {
    const course = sequelize.define('course', {
        course_template: DataTypes.INTEGER,
        commencing_on: DataTypes.DATE,
        reference_text: DataTypes.STRING,
        studio: DataTypes.INTEGER,
        facility: DataTypes.INTEGER,
        schedule: DataTypes.STRING,
        max_student: DataTypes.INTEGER,
        min_student: DataTypes.INTEGER,
        course_fee_type: DataTypes.STRING,
        expiry_date: DataTypes.INTEGER,
        required_confirmation: DataTypes.TINYINT,
        tutor: DataTypes.INTEGER
    }, {});
    course.associate = function(models) {
        // associations can be defined here
        course.belongsTo(models.facility, {
            foreignKey: 'facility',
            as: 'facilityInformation'
        })
        course.belongsTo(models.course_template, {
            foreignKey: 'course_template',
            as: 'courseTemplateInformation'
        })
        course.belongsTo(models.studio, {
            foreignKey: 'studio',
            as: 'studioInformation'
        })
        course.hasMany(models.course_enrolment, {
            foreignKey: 'course_id',
            as: 'enrolmentRecord'
        })
        course.hasMany(models.class, {
            foreignKey: 'course',
                as: 'classRecord'
        })
    };
    return course;
};