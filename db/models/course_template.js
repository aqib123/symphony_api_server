'use strict';
module.exports = (sequelize, DataTypes) => {
    const course_template = sequelize.define('course_template', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        payment_mode: DataTypes.TINYINT,
        group_individual: DataTypes.TINYINT,
        studio: DataTypes.INTEGER,
        currency: DataTypes.INTEGER,
        price: DataTypes.DECIMAL,
        category: DataTypes.INTEGER,
        sub_category: DataTypes.INTEGER,
        number_of_class: DataTypes.INTEGER,
        class_duration: DataTypes.INTEGER,
        course_image: DataTypes.STRING,
        course_requirement: DataTypes.STRING
    }, {});
    course_template.associate = function(models) {
        // associations can be defined here
        course_template.hasMany(models.course, {
            foreignKey: 'course_template',
            as: 'courses'
        })
        course_template.belongsTo(models.category, {
            foreignKey: 'category',
            as: 'categoryInformation'
        })
        course_template.belongsTo(models.sub_category, {
            foreignKey: 'sub_category',
            as: 'subCategoryInformation'
        })
        course_template.belongsTo(models.currency, {
            foreignKey: 'currency',
            as: 'currencyInformation'
        })
        course_template.belongsTo(models.studio, {
            foreignKey: 'studio',
            as: 'studioInformation'
        })
    };
    return course_template;
};