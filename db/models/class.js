'use strict';
module.exports = (sequelize, DataTypes) => {
    const classe = sequelize.define('class', {
        course: DataTypes.INTEGER,
        time: DataTypes.INTEGER,
        tutor: DataTypes.INTEGER,
        studio: DataTypes.INTEGER
    }, {});
    classe.associate = function(models) {
        // associations can be defined here
    };
    return classe;
};