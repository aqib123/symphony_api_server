'use strict';
module.exports = (sequelize, DataTypes) => {
    const studio = sequelize.define('studio', {
        name: DataTypes.STRING,
        owner: DataTypes.INTEGER,
        type: DataTypes.TINYINT,
        studio_logo: DataTypes.STRING,
        studio_banner: DataTypes.STRING
    }, {});
    studio.associate = function(models) {
        // associations can be defined here
        studio.hasMany(models.studio_center, {
            foreignKey: 'studio',
            as: 'studioCenterRecord'
        })
        studio.hasMany(models.studio_tutor, {
            foreignKey: 'studio',
            as: 'tutorRecord'
        })
        studio.hasMany(models.course_template, {
            foreignKey: 'studio',
            as: 'courseTemplateRecord'
        })
        studio.hasMany(models.course, {
            foreignKey: 'studio',
            as: 'coursesRecord'
        })
    };
    return studio;
};