'use strict';
module.exports = (sequelize, DataTypes) => {
    const facility = sequelize.define('facility', {
        studio_center: DataTypes.INTEGER,
        name: DataTypes.STRING,
        capacity: DataTypes.INTEGER
    }, {});
    facility.associate = function(models) {
        // associations can be defined here
        facility.belongsTo(models.studio_center, {
            foreignKey: 'studio_center',
            as: 'studioCenterRecord'
        })
    };
    return facility;
};