'use strict';
module.exports = (sequelize, DataTypes) => {
  const address = sequelize.define('address', {
    line_1: DataTypes.STRING,
    line_2: DataTypes.STRING,
    city: DataTypes.STRING,
    postcode: DataTypes.STRING,
    province: DataTypes.STRING,
    country: DataTypes.STRING,
    latitude: DataTypes.DECIMAL,
    longitude: DataTypes.DECIMAL
  }, {});
  address.associate = function(models) {
    // associations can be defined here
  };
  return address;
};