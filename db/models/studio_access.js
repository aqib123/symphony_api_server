'use strict';
module.exports = (sequelize, DataTypes) => {
  const studio_access = sequelize.define('studio_access', {
    staff: DataTypes.INTEGER,
    studio: DataTypes.INTEGER,
    access_level: DataTypes.INTEGER
  }, {});
  studio_access.associate = function(models) {
    // associations can be defined here
  };
  return studio_access;
};