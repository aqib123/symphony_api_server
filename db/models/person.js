'use strict';
module.exports = (sequelize, DataTypes) => {
    const person = sequelize.define('person', {
        gender: DataTypes.TINYINT,
        dob: DataTypes.INTEGER,
        can_login: DataTypes.TINYINT,
        address: DataTypes.INTEGER,
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
        deletedAt: DataTypes.DATE
    }, {});
    person.associate = function(models) {
        // associations can be defined here
        person.hasMany(models.guardian_student, {
            foreignKey: 'guardian',
            as: 'studentRecords'
        })
        person.hasMany(models.course_enrolment, {
            foreignKey: 'student',
            as: 'courses'
        })
    };
    return person;
};