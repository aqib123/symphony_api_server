'use strict';
module.exports = (sequelize, DataTypes) => {
  const transaction = sequelize.define('transaction', {
    time: DataTypes.INTEGER,
    madeby: DataTypes.INTEGER,
    description: DataTypes.STRING,
    currency: DataTypes.INTEGER,
    amount: DataTypes.DECIMAL,
    course: DataTypes.INTEGER,
    student: DataTypes.INTEGER
  }, {});
  transaction.associate = function(models) {
    // associations can be defined here
  };
  return transaction;
};