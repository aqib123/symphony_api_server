'use strict';
module.exports = (sequelize, DataTypes) => {
    const studio_tutor = sequelize.define('studio_tutor', {
        studio: DataTypes.INTEGER,
        tutor: DataTypes.INTEGER,
        start_date: DataTypes.INTEGER
    }, {});
    studio_tutor.associate = function(models) {
        // associations can be defined here
        studio_tutor.belongsTo(models.person, {
            foreignKey: 'tutor',
            as: 'personalInformation'
        })
        studio_tutor.hasMany(models.class, {
            foreignKey: 'tutor',
                as: 'classes',
                targetKey: 'tutor'
        })
    };
    return studio_tutor;
};