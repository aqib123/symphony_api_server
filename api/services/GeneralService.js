/**
 *
 * Dependencies
 *
 */
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
let moment = require("moment-timezone");
var db = new(require('../../db_mysql.js'));
var GuardianStudent = require('../../db/models').guardian_student;
var CourseEnrollment = require('../../db/models').course_enrolment;
var Currency = require('../../db/models').currency;
var Studio = require('../../db/models').studio;
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.COMPANY_EMAIL_ADDRESS,
        pass: password
    }
});
var Login = require('../../db/models').login;
const EmailTemplate = require('email-templates');
var path = require('path');
var Promise = require('bluebird');
var sender = 'smtps://' + process.env.MAIL_USERNAME; // The emailto use in sending the email
//(Change the @ symbol to %40 or do a url encoding )
var password = process.env.MAIL_PASSWORD;
let users = [{
    name: 'Jack',
    email: 'example@example.tld',
}, {
    name: 'John',
    email: 'example@example.tld',
}, {
    name: 'Joe',
    email: 'example@example.tld',
}, ];

function sendEmail(obj) {
    return transporter.sendMail(obj);
}

function loadTemplate(templateName, contexts) {
    let template = new EmailTemplate(path.join(__dirname, 'templates', templateName));
    return Promise.all(contexts.map((context) => {
        return new Promise((resolve, reject) => {
            template.render(context, (err, result) => {
                if (err) reject(err);
                else resolve({
                    email: result,
                    context,
                });
            });
        });
    }));
}
/*--------- Creating 5 Digit OTP Code for Varification  ----------*/
exports.GetOTPCode = function() {
    var min = 10000;
    var max = 99999;
    var uniqueCode = Math.floor(Math.random() * (+max - +min)) + +min;
    return uniqueCode;
}
/*----------  Send Email Data  ----------*/
exports.SendEmail = function(email, optcode) {
    loadTemplate('send_code', users).then((results) => {
        return Promise.all(results.map((result) => {
            sendEmail({
                to: result.context.email,
                from: process.env.COMPANY_EMAIL_ADDRESS,
                subject: result.email.subject,
                html: result.email.html,
                text: result.email.text,
            });
        }));
    }).then(() => {
        console.log('Yay!');
    });
}
/*----------  Section for Defining Header  ----------*/
exports.JwtHeader = function(userId, personId, studioId) {
    var object = {
        "userId": userId,
        "personId": personId,
        "studioId": studioId
    }
    var token = jwt.sign(object, process.env.JWT_SECRET, {
        expiresIn: 86400 // expires in 24 hours
    });
    var header = {
        "Authorization": token,
    };
    return header;
}
/*----------  Decode Authorization Code  ----------*/
exports.JwtHeaderResponse = function(token, callback) {
    // the synchronous code that we want to catch thrown errors on
    jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(decoded);
        }
    });
}
/*----------  Alter Validation Error  ----------*/
exports.ChangeValidationErrorObject = function(code, field) {
    return errorResponse = {
        "code": code,
        "field": field,
        "status": 0,
    }
}
/*----------  Check if age is greater than 10 Years  ----------*/
exports.CheckAge = function(date) {
    var dateInFormat = moment.unix(date).format("YYYY-MM-DD");
    var years = moment().diff(dateInFormat, 'years', false);
    if (years <= 10) {
        return true;
    } else {
        return false;
    }
}
/*----------  Get Timezone date times  ----------*/
exports.GetRelatedDateTime = function(datetime, timezone) {
    return moment.tz(datetime, timezone).format("DD/MM/YYYY HH:mm:ss");
}
/*----------  Check if User exist in system  ----------*/
exports.UserExist = function(userId, callback) {
    sql1 = "SELECT id FROM logins WHERE id = ?";
    db.query(sql1, [userId], function(sql1Result, err1) {
        if (sql1Result.length > 0) {
            callback(true);
        } else {
            callback(false);
        }
    });
}
/*----------  Check if the students guardian is same  ----------*/
exports.CheckParentStudents = function(parentId, studentArray, callback) {
    var students = [];
    students = studentArray.concat();
    
    GuardianStudent.findAll({
        where: {
            student: {
                $in: students
            },
            guardian: parentId
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            // Check which stduent does not come under under that parent or not.
            sql1Result.forEach(function(record) {
                if (students.includes(record.student) == true) {
                    
                    var index = students.indexOf(record.student)
                    if (index !== -1) students.splice(index, 1)
                }
            });
            callback(students)
        } else {
            callback([])
        }
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/*----------  Check if Student Already register with the given course id  ----------*/
exports.CheckStudentExistWithCourse = function(courseId, studentArray, callback) {
    var students = [];
    var ListOfAlreadyExistedStudents = [];
    students = studentArray.concat();
    CourseEnrollment.findAll({
        where: {
            student: {
                $in: students
            },
            course_id: courseId
        }
    }).then(sql1Result => {
        console.log(sql1Result)
        if (sql1Result.length > 0) {
            // Check which stduent does not come under under that parent or not.
            sql1Result.forEach(function(record) {
                if (students.includes(record.student) == true) {
                    ListOfAlreadyExistedStudents.push(record.student);
                }
            });
            callback(ListOfAlreadyExistedStudents)
        } else {
            callback([])
        }
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/*----------  Check if the Currency given exist in the system  ----------*/
exports.CheckCurrencyExist = function(currencyId, callback) {
    Currency.findAll({
        where: {
            id: currencyId
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            callback(true)
        } else {
            callback(false)
        }
    }).catch(function(err1) {
        if (err1) callback(false)
    })
}
/*----------  Check Parent Id and check if token has same user id that is sent  ----------*/
exports.CheckUserExistWithToken = function(id, token, callback) {
    // Check Token has same userId
    this.JwtHeaderResponse(token, function(response) {
        if (response) {
            if (id == response.userId) {
                callback(true)
            } else {
                callback(false)
            }
        } else {
            callback(false)
        }
    })
}