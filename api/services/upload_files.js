require('dotenv').config();
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS,
    accessKeyId: process.env.AWS_ACCESS_KEY,
    region: 'ap-southeast-1'
});
const s3 = new aws.S3();
const fileFilter = (req, file, cb) => {
    if (req.body.studio_id != undefined) {
        console.log(req.body.studio_id)
        if (req.body.logo_delete != undefined && req.body.banner_delete != undefined) {
            if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
                cb(null, true)
            } else {
                cb(JSON.stringify({
                    'status': 0,
                    code: 223,
                }), false);
            }
        } else {
            cb(JSON.stringify({
                'status': 0,
                code: 290,
            }), false);
        }
    } else {
        cb(JSON.stringify({
            'status': 0,
            code: 291,
        }), false);
    }
}
const upload = multer({
    fileFilter,
    storage: multerS3({
        s3,
        bucket: 'symone',
        acl: 'public-read',
        metadata: function(req, file, cb) {
            cb(null, {
                fieldName: 'TESTING_META_DATA!'
            });
        },
        key: function(req, file, cb) {
            var newFileName = Date.now() + "-" + file.originalname;
            var fullPath = 'uploads/logo/' + newFileName;
            cb(null, fullPath)
        }
    })
})
module.exports = upload;