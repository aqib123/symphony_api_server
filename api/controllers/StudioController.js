/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var generalService = require('../services/GeneralService.js');
var Person = require('../../db/models').person;
var GuardianStudent = require('../../db/models').guardian_student;
var CourseTemplate = require('../../db/models').course_template;
var Course = require('../../db/models').course;
var parser = require('cron-parser');
var Class = require('../../db/models').class;
var Category = require('../../db/models').category;
var SubCategory = require('../../db/models').sub_category;
var Currency = require('../../db/models').currency;
var Studio = require('../../db/models').studio;
var StudioCenter = require('../../db/models').studio_center;
var StudioTutor = require('../../db/models').studio_tutor;
var Facility = require('../../db/models').facility;
var Address = require('../../db/models').address;
var CourseEnrollement = require('../../db/models').course_enrolment;
var db = new(require('../../db_mysql.js'));
// const upload = require('../services/upload_files');
var app = require('../../db/models')
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS,
    accessKeyId: process.env.AWS_ACCESS_KEY,
    region: 'ap-southeast-1'
});
const s3 = new aws.S3();
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true)
    } else {
        cb({
            'status': 0,
            code: 223,
            'msg': 'Invalid Mime Type, only JPEG and PNG'
        }, false);
    }
}
var upload = multer({
    fileFilter,
    storage: multerS3({
        s3,
        bucket: 'symone',
        acl: 'public-read',
        metadata: function(req, file, cb) {
            cb(null, {
                fieldName: 'TESTING_META_DATA!'
            });
        },
        key: function(req, file, cb) {
            var newFileName = Date.now() + "-" + file.originalname;
            var fullPath = 'uploads/logo/' + newFileName;
            cb(null, fullPath)
        }
    })
}).fields([{
    name: 'logo',
}, {
    name: 'banner',
}])
/*----------  Implementation of Student Controller Logic  ----------*/
/**
 *
 * Create Course Template
 *
 */
exports.CreateCourseTemplate = function(req, res) {
    // Validation
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(236, 'name'));
    // if (req.validationErrors(req.checkBody("description").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(237, 'description'));
    if (req.validationErrors(req.checkBody("payment_mode").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(238, 'payment_mode'));
    if (req.validationErrors(req.checkBody("group_individual").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(239, 'group_individual'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("currency").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(230, 'currency'));
    if (req.validationErrors(req.checkBody("price").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(241, 'price'));
    if (req.validationErrors(req.checkBody("category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(242, 'category'));
    if (req.validationErrors(req.checkBody("category").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(245, 'category'));
    if (req.validationErrors(req.checkBody("sub_category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(243, 'sub_category'));
    if (req.validationErrors(req.checkBody("sub_category").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(244, 'sub_category'));
    if (req.validationErrors(req.checkBody("class_duration").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(246, 'class_duration'));
    if (req.validationErrors(req.checkBody("class_duration").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(247, 'class_duration'));
    if (req.validationErrors(req.checkBody("number_of_class").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(262, 'number_of_class'));
    if (req.validationErrors(req.checkBody("number_of_class").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(263, 'number_of_class'));
    /**
     *
     * First Check if the Record already Existed with same studio
     *
     */
    CourseTemplate.findAll({
        where: {
            studio: req.body.studio,
            name: req.body.name,
            category: req.body.category,
            sub_category: req.body.sub_category,
            currency: req.body.currency,
            group_individual: req.body.group_individual
        }
    }).then(responseRecord => {
        if (responseRecord.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            CourseTemplate.create(req.body).then(function(response) {
                getCourseTemplateRecord(req.body.studio, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    });
                });
            }).catch(function(err2) {
                // Through DB related issue Errors
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Update Course Template Record
 *
 */
exports.UpdateCourseTemplate = function(req, res) {
    // Validation
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(236, 'name'));
    // if (req.validationErrors(req.checkBody("description").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(237, 'description'));
    if (req.validationErrors(req.checkBody("payment_mode").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(238, 'payment_mode'));
    if (req.validationErrors(req.checkBody("group_individual").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(239, 'group_individual'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("currency").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(230, 'currency'));
    if (req.validationErrors(req.checkBody("price").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(241, 'price'));
    if (req.validationErrors(req.checkBody("category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(242, 'category'));
    if (req.validationErrors(req.checkBody("category").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(245, 'category'));
    if (req.validationErrors(req.checkBody("sub_category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(243, 'sub_category'));
    if (req.validationErrors(req.checkBody("sub_category").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(244, 'sub_category'));
    if (req.validationErrors(req.checkBody("class_duration").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(246, 'class_duration'));
    if (req.validationErrors(req.checkBody("class_duration").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(247, 'class_duration'));
    if (req.validationErrors(req.checkBody("course_template_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(250, 'course_template_id'));
    if (req.validationErrors(req.checkBody("number_of_class").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(262, 'number_of_class'));
    if (req.validationErrors(req.checkBody("number_of_class").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(263, 'number_of_class'));
    CourseTemplate.findAll({
        where: {
            studio: req.body.studio,
            name: req.body.name,
            category: req.body.category,
            sub_category: req.body.sub_category,
            currency: req.body.currency,
            group_individual: req.body.group_individual,
            id: {
                $not: req.body.course_template_id
            }
        }
    }).then(responseRecord => {
        if (responseRecord.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            CourseTemplate.update(req.body, {
                where: {
                    id: req.body.course_template_id
                }
            }).then(sql2Result => {
                getCourseTemplateRecord(req.body.studio, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    });
                });
            }).catch(function(err2) {
                // Through DB related issue Errors
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Course Template Record
 *
 */
exports.DeleteCourseTemplate = function(req, res) {
    if (req.validationErrors(req.checkBody("course_template_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(250, 'course_template_id'));
    // Check if the Course Template is reffered in other table
    CourseTemplate.destroy({
        where: {
            id: req.body.course_template_id
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getCourseTemplateRecord(req.body.studio, function(responseRecord) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": responseRecord
                });
            });
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Get All Course Templates
 *
 */
exports.CourseTemplateRecord = function(req, res) {
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    getCourseTemplateRecord(req.body.studio, function(responseRecord) {
        return res.json({
            "status": 1,
            "message": 'success',
            "code": 200,
            "record": responseRecord
        });
    });
}
/**
 *
 * Function for Retrieving Course Templates
 *
 */
function getCourseTemplateRecord(studioId, callback) {
    // Get Active Student Record
    CourseTemplate.findAll({
        where: {
            studio: studioId,
        },
    }).then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/**
 *
 * Create Course Record
 *
 */
exports.CreateCourse = function(req, res) {
    // Validation
    if (req.validationErrors(req.checkBody("course_template").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(250, 'course_template'));
    // if (req.validationErrors(req.checkBody("description").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(237, 'description'));
    if (req.validationErrors(req.checkBody("commencing_on").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(264, 'commencing_on'));
    if (req.validationErrors(req.checkBody("reference_text").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(265, 'reference_text'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(253, 'studio'));
    if (req.validationErrors(req.checkBody("facility").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(266, 'facility'));
    if (req.validationErrors(req.checkBody("schedule").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(267, 'schedule'));
    if (req.validationErrors(req.checkBody("max_student").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(268, 'max_student'));
    if (req.validationErrors(req.checkBody("max_student").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(269, 'max_student'));
    if (req.validationErrors(req.checkBody("min_student").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(270, 'min_student'));
    if (req.validationErrors(req.checkBody("min_student").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(271, 'min_student'));
    if (req.validationErrors(req.checkBody("required_confirmation").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(272, 'required_confirmation'));
    if (req.validationErrors(req.checkBody("tutor").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(273, 'tutor'));
    if (req.validationErrors(req.checkBody("expiry_date").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(293, 'expiry_date'));
    if (req.validationErrors(req.checkBody("expiry_date").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(294, 'expiry_date'));
    if (req.validationErrors(req.checkBody("expiry_date").isLength({
            min: 1,
            max: 11
        })) != false) return res.send(generalService.ChangeValidationErrorObject(295, 'expiry_date'));
    /**
     *
     * First Check if the Record already Existed with same studio
     *
     */
    req.body.commencing_on = new Date(req.body.commencing_on)
    Course.findAll({
        where: {
            studio: req.body.studio,
            course_template: req.body.course_template,
            facility: req.body.facility,
            schedule: req.body.schedule,
            max_student: req.body.max_student
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 248,
                "status": 0,
            });
        } else {
            Course.create(req.body).then(function(courseRecord) {
                // Get Number of Classes from CourseTemplate
                CourseTemplate.findOne({
                    id: req.body.course_template
                }).then(function(courseTemplateRecord) {
                    // On Course Create We need to create the Classes on basis of the Cron String
                    var arrayRecord = []
                    arrayRecord = getParseRecordFromCronString(req.body.schedule, courseTemplateRecord.number_of_class, courseRecord.id, req.body.tutor, req.body.studio)
                    if (arrayRecord.length > 0) {
                        Class.bulkCreate(arrayRecord).then(sql2Result => {
                            getCourseRecord(req.body.studio, function(responseRecord) {
                                return res.json({
                                    "status": 1,
                                    "message": 'success',
                                    "code": 200,
                                    "record": responseRecord
                                });
                            });
                        }).catch(function(err4) {
                            // Through DB related issue Errors
                            if (err4) return res.json(err4)
                        })
                    }
                }).catch(function(err3) {
                    // Through DB related issue Errors
                    if (err3) return res.json(err3)
                })
            }).catch(function(err2) {
                // Through DB related issue Errors
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Create Course Record
 *
 */
exports.UpdateCourse = function(req, res) {
    // Validation
    // if (req.validationErrors(req.checkBody("course_template").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(250, 'course_template'));
    if (req.validationErrors(req.checkBody("course_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(228, 'course_id'));
    if (req.validationErrors(req.checkBody("course_template").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(250, 'course_template'));
    // if (req.validationErrors(req.checkBody("description").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(237, 'description'));
    if (req.validationErrors(req.checkBody("commencing_on").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(264, 'commencing_on'));
    if (req.validationErrors(req.checkBody("reference_text").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(265, 'reference_text'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(253, 'studio'));
    if (req.validationErrors(req.checkBody("facility").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(266, 'facility'));
    // if (req.validationErrors(req.checkBody("schedule").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(267, 'schedule'));
    if (req.validationErrors(req.checkBody("max_student").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(268, 'max_student'));
    if (req.validationErrors(req.checkBody("max_student").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(269, 'max_student'));
    if (req.validationErrors(req.checkBody("min_student").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(270, 'min_student'));
    if (req.validationErrors(req.checkBody("min_student").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(271, 'min_student'));
    if (req.validationErrors(req.checkBody("required_confirmation").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(272, 'required_confirmation'));
    if (req.validationErrors(req.checkBody("tutor").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(273, 'tutor'));
    if (req.validationErrors(req.checkBody("expiry_date").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(293, 'expiry_date'));
    if (req.validationErrors(req.checkBody("expiry_date").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(294, 'expiry_date'));
    if (req.validationErrors(req.checkBody("expiry_date").isLength({
            min: 1,
            max: 11
        })) != false) return res.send(generalService.ChangeValidationErrorObject(295, 'expiry_date'));
    /**
     *
     * First Check if the Record already Existed with same studio
     *
     */
    delete req.body.schedule
    req.body.commencing_on = new Date(req.body.commencing_on)
    Course.findAll({
        where: {
            id: req.body.course_id
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            Course.update(req.body, {
                where: {
                    course: req.body.course_id
                }
            }).then(sql2Result => {
                // Update the tutor in the Course Record as well
                Class.update({
                    studio: req.body.studio,
                    tutor: req.body.tutor
                }, {
                    where: {
                        course: req.body.course_id
                    }
                }).then(sql3Result => {
                    getCourseRecord(req.body.studio, function(responseRecord) {
                        return res.json({
                            "status": 1,
                            "message": 'success',
                            "code": 200,
                            "record": responseRecord
                        });
                    });
                }).catch(function(err3) {
                    // Through DB related issue Errors
                    if (err3) return res.json(err3)
                })
                getCourseTemplateRecord(req.body.studio, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    });
                });
            }).catch(function(err2) {
                // Through DB related issue Errors
                if (err2) return res.json(err2)
            })
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Course Record
 *
 */
exports.DeleteCourse = function(req, res) {
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("course_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(228, 'course_template_id'));
    // Check if the Course Template is reffered in other table
    Course.destroy({
        where: {
            studio: req.body.studio,
            id: req.body.course_id
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getCourseTemplateRecord(req.body.studio, function(responseRecord) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": responseRecord
                });
            });
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Get All Course Templates
 *
 */
exports.CourseRecord = function(req, res) {
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    getCourseRecord(req.body.studio, function(responseRecord) {
        return res.json({
            "status": 1,
            "message": 'success',
            "code": 200,
            "record": responseRecord
        });
    });
}
/**
 *
 * Function for Retrieving Courses
 *
 */
function getCourseRecord(studioId, callback) {
    // Get Active Student Record
    Course.findAll({
        where: {
            studio: studioId
        },
    }).then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/**
 *
 * Function for Parsing the Cron String and return the unixTime Record for the Classes
 *
 */
function getParseRecordFromCronString(cronString, numberOfClasses, courseId, personId, studioId) {
    var classDates = [];
    var interval = parser.parseExpression(cronString);
    if (numberOfClasses > 0) {
        for (var i = 0; i < numberOfClasses; i++) {
            classDates.push({
                'course': courseId,
                'time': Math.round((new Date(interval.next().toString())).getTime() / 1000),
                'tutor': personId,
                'studio': studioId
            })
        }
    }
    return classDates
}
/*================================================================
=            Section for Category CRUD Implementation            =
================================================================*/
/**
 *
 * Category Create Record
 *
 */
exports.CreateCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(242, 'name'));
    Category.findAll({
        where: {
            name: req.body.name,
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Category.create(req.body).then(function(sql2Result) {
                getCategoryRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Update Category Record
 *
 */
exports.UpdateCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(242, 'name'));
    if (req.validationErrors(req.checkBody("category_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(274, 'category_id'));
    Category.findAll({
        where: {
            name: req.body.name,
            id: {
                $not: req.body.category_id
            }
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Category.update(req.body, {
                where: {
                    id: req.body.category_id
                }
            }).then(function(sql2Result) {
                getCategoryRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Category Record
 *
 */
exports.DeleteCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("category_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(274, 'category_id'));
    // Check if the Course Template is reffered in other table
    Category.destroy({
        where: {
            id: req.body.category_id
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getCategoryRecord(function(record) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": record
                });
            })
        } else {
            return res.json({
                "code": 249,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Get All Categories
 *
 */
exports.CategoryRecord = function(req, res) {
    getCategoryRecord(function(record) {
        return res.json({
            "status": 1,
            "message": 'success',
            "code": 200,
            "record": record
        });
    })
}
/**
 *
 * Get All categories
 *
 */
function getCategoryRecord(callback) {
    // Get Active Student Record
    Category.findAll().then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/*=====  End of Section for Category CRUD Implementation  ======*/
/*=======================================================
=            Section for Sub Category Record            =
=======================================================*/
/**
 *
 * Sub Category Create Record
 *
 */
exports.CreateSubCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(243, 'name'));
    if (req.validationErrors(req.checkBody("category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(274, 'category'));
    SubCategory.findAll({
        where: {
            name: req.body.name,
            category: req.body.category,
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            SubCategory.create(req.body).then(function(sql2Result) {
                getSubCategoryRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Update Sub Category Record
 *
 */
exports.UpdateSubCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(242, 'name'));
    if (req.validationErrors(req.checkBody("category").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(274, 'category'));
    if (req.validationErrors(req.checkBody("sub_category_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(275, 'sub_category_id'));
    SubCategory.findAll({
        where: {
            name: req.body.name,
            category: req.body.category,
            id: {
                $not: req.body.sub_category_id
            }
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            SubCategory.update(req.body, {
                where: {
                    id: req.body.sub_category_id
                }
            }).then(function(sql2Result) {
                getSubCategoryRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Category Record
 *
 */
exports.DeleteSubCategory = function(req, res) {
    if (req.validationErrors(req.checkBody("sub_category_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(275, 'sub_category_id'));
    // Check if the Course Template is reffered in other table
    SubCategory.destroy({
        where: {
            id: req.body.sub_category_id
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getSubCategoryRecord(function(record) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": record
                });
            })
        } else {
            return res.json({
                "code": 228,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Get All Sub Categories
 *
 */
exports.SubCategoryRecord = function(req, res) {
    getSubCategoryRecord(function(record) {
        return res.json({
            "status": 1,
            "message": 'success',
            "code": 200,
            "record": record
        });
    })
}
/**
 *
 * Get All sub categories
 *
 */
function getSubCategoryRecord(callback) {
    // Get Active Student Record
    SubCategory.findAll().then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/*=====  End of Section for Sub Category Record  ======*/
/*===================================================
=            Section for Currency Record            =
===================================================*/
/**
 *
 * Currency Create Record
 *
 */
exports.CreateCurrency = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(230, 'name'));
    if (req.validationErrors(req.checkBody("code").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(276, 'code'));
    Currency.findAll({
        where: {
            name: req.body.name,
            code: req.body.code
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Currency.create(req.body).then(function(sql2Result) {
                getCurrencyRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Update Currency Record
 *
 */
exports.UpdateCurrency = function(req, res) {
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(230, 'name'));
    if (req.validationErrors(req.checkBody("code").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(276, 'code'));
    if (req.validationErrors(req.checkBody("currency_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(277, 'currency_id'));
    Currency.findAll({
        where: {
            name: req.body.name,
            code: req.body.code,
            id: {
                $not: req.body.currency_id
            }
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Currency.update(req.body, {
                where: {
                    id: req.body.currency_id
                }
            }).then(function(sql2Result) {
                getCurrencyRecord(function(record) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": record
                    });
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Currency Record
 *
 */
exports.DeleteCurrency = function(req, res) {
    if (req.validationErrors(req.checkBody("currency_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(277, 'currency_id'));
    // Check if the Course Template is reffered in other table
    Currency.destroy({
        where: {
            id: req.body.currency_id
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getCurrencyRecord(function(record) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": record
                });
            })
        } else {
            if (err1) return res.json({
                "code": 228,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Get All Currencies
 *
 */
exports.CurrencyRecord = function(req, res) {
    getCurrencyRecord(function(record) {
        return res.json({
            "status": 1,
            "message": 'success',
            "code": 200,
            "record": record
        });
    })
}
/**
 *
 * Get All Currencies
 *
 */
function getCurrencyRecord(callback) {
    // Get Active Student Record
    Currency.findAll().then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}
/*=====  End of Section for Currency Record  ======*/
/*============================================
=            Studio Related Api's            =
============================================*/
exports.GetAllStudioRecord = function(req, res) {
    // Get All STudio Records.
    var studio;
    var orderBy = 'ASC';
    var whereObject = {};
    if (req.query.studio_id != undefined) {
        whereObject = {
            id: req.query.studio_id
        };
    }
    if (req.query.order_by != undefined) {
        orderBy = req.query.order_by
        if (orderBy != 'ASC' || orderBy != 'DESC') {
            orderBy = 'ASC'
        }
    }
    Studio.findAll({
        where: whereObject,
        order: [
            ['id', orderBy],
        ],
        include: [{
            // all: true,
            model: StudioCenter,
            as: 'studioCenterRecord',
            include: [{
                // all: true,
                model: Address,
                as: 'addressInformation',
            }]
        }, {
            // all: true,
            model: StudioTutor,
            nested: true,
            as: 'tutorRecord',
            include: [{
                // all: true,
                model: Person,
                as: 'personalInformation'
            }],
        }, {
            // all: true,
            model: CourseTemplate,
            nested: true,
            as: 'courseTemplateRecord',
            include: [{
                // all: true,
                model: Course,
                as: 'courses',
                order: [
                    ['id', 'DESC'],
                ],
                include: [{
                    // all: true,
                    model: Facility,
                    as: 'facilityInformation',
                }, {
                    // all: true,
                    model: CourseEnrollement,
                    as: 'enrolmentRecord',
                    include: [{
                        model: Person,
                        as: 'StudentEnrolledRecord',
                    }],
                }]
            }, {
                // all: true,
                model: Category,
                as: 'categoryInformation',
            }, {
                // all: true,
                model: SubCategory,
                as: 'subCategoryInformation',
            }, {
                // all: true,
                model: Currency,
                as: 'currencyInformation',
            }],
        }],
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Get All Studio Teachers and courses assigned to them
 *
 */
exports.GetAllTutors = function(req, res) {
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    var orderBy = 'ASC';
    if (req.body.order_by != undefined) {
        orderBy = req.query.order_by
        if (orderBy != 'ASC' || orderBy != 'DESC') {
            orderBy = 'ASC'
        }
    }
    // Get All tutor Record
    Studio.findAll({
        where: {
            id: req.body.studio
        },
        order: [
            ['createdAt', orderBy],
        ],
        include: [{
            model: StudioTutor,
            nested: true,
            as: 'tutorRecord',
            include: [{
                all: true,
                model: Person,
                as: 'personalInformation'
            }, {
                all: true,
                model: Class,
                as: 'classes'
            }],
        }],
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Get All Studio Centers Based on the Studio id
 *
 */
exports.GetAllStudioCenters = function(req, res) {
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    var orderBy = 'ASC';
    if (req.body.order_by != undefined) {
        orderBy = req.body.order_by
        if (orderBy != 'ASC' || orderBy != 'DESC') {
            orderBy = 'ASC'
        }
    }
    // Get All Studio Centers
    Studio.findAll({
        where: {
            id: req.body.studio
        },
        order: [
            ['createdAt', orderBy],
        ],
        include: [{
            model: StudioCenter,
            as: 'studioCenterRecord',
        }, {
            model: StudioTutor,
            nested: true,
            as: 'tutorRecord',
            include: [{
                all: true,
                model: Person,
                as: 'personalInformation'
            }, {
                all: true,
                model: Class,
                as: 'classes'
            }],
        }],
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Test Token
 *
 */
exports.testToken = function(req, res) {
    return res.json('here')
}
/**
 *
 * Get Studio Information
 *
 */
exports.GetStudioInformation = function(req, res) {
    if (req.validationErrors(req.checkBody("person_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(283, 'person_id'));
    // Add Raw Query
    var sql1 = "select *, (select COUNT(*) from courses c where c.studio=s.id) coursesCount from studios s WHERE s.owner = ?"
    // var sql1 = 'SELECT s.id, s.name, s.owner, s.type, s.studio_logo, s.studio_banner, s.createdAt, s.updatedAt, count(c.id) as courseCount FROM studios s LEFT JOIN courses c ON s.id = courses.id WHERE s.owner = ? group by c.id';
    db.query(sql1, [req.body.person_id], function(sql1Result, err1) {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    })
    // Studio.findAll({
    //     where: {
    //         owner: req.body.person_id,
    //     },
    //     include: [{
    //         model: Course,
    //         as: 'coursesRecord',
    //     }],
    //     group: ['studios.id']
    // }).then(sql1Result => {
    //     if (sql1Result.length > 0) {
    //         return res.json({
    //             "status": 1,
    //             "message": 'success',
    //             "code": 200,
    //             "record": sql1Result
    //         })
    //     } else {
    //         return res.json({
    //             "status": 0,
    //             "code": 223
    //         })
    //     }
    // }).catch(function(err1) {
    //     if (err1) return res.json(err1)
    // })
}
/**
 *
 * Switch Account from Parent to Studio Account (Register as Studio)
 *
 */
exports.RegisterAsStudio = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("person_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(283, 'person_id'));
    if (req.validationErrors(req.checkBody("studio_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(211, 'studio_name'));
    Studio.findOne({
        where: {
            owner: req.body.person_id
        },
    }).then(sql1Result => {
        if (sql1Result) {
            return res.json({
                "status": 0,
                "code": 278
            })
        } else {
            // 1) If there is no record register for studio. and account was parent account before. we will switch account to studio
            Studio.create({
                'name': req.body.studio_name,
                'owner': req.body.person_id,
                'type': 1,
            }).then(sql2Result => {
                getStudioInformation(sql2Result.id, function(response) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": response
                    })
                })
            }).catch(function(err1) {
                if (err1) return res.json(err1)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Upload Studio Files
 *
 */
exports.UploadStudioFiles = function(req, res) {
    if (req.files) {
        var resultData = {}
        // Update Record in the given Studio
        if (req.files.logo != undefined) {
            resultData.studio_logo = req.files.logo[0].location
        }
        if (req.files.banner != undefined) {
            resultData.studio_banner = req.files.banner[0].location
        }
        if (req.body.logo_delete == 1) {
            resultData.studio_logo = null
        }
        if (req.body.banner_delete == 1) {
            resultData.studio_banner = null
        }
        Studio.update(resultData, {
            where: {
                id: req.body.studio_id,
            }
        }).then(sql2Result => {
            getStudioInformation(req.body.studio_id, function(response) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": response
                })
            })
        }).catch(function(err1) {
            if (err1) return res.json(err1)
        })
    } else {
        return res.json({
            "status": 0,
            "code": 223
        })
    }
}
/**
 *
 * Update Studio Profile
 *
 */
exports.UpdateStudioProfile = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("person_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(283, 'person_id'));
    if (req.validationErrors(req.checkBody("studio_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(211, 'studio_name'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    Studio.findOne({
        where: {
            owner: req.body.person_id,
            id: req.body.studio
        },
    }).then(sql1Result => {
        if (sql1Result) {
            Studio.update({
                'name': req.body.studio_name,
                'owner': req.body.person_id,
                'type': 1,
            }, {
                where: {
                    owner: req.body.person_id,
                }
            }).then(sql2Result => {
                getStudioInformation(req.body.studio, function(response) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": response
                    })
                })
            }).catch(function(err1) {
                if (err1) return res.json(err1)
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Get Studio Information
 *
 */
function getStudioInformation(studioId, callback) {
    Studio.findOne({
        where: {
            id: studioId
        }
    }).then(sql1Result => {
        if (sql1Result) {
            callback(sql1Result)
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete the upload file
 *
 */
/*=====  End of Studio Related Api's  ======*/