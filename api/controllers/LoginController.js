/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var db = new(require('../../db_mysql.js'));
// var db = require('../../db_connection.js');
var generalService = require('../services/GeneralService.js');
const bcrypt = require('bcrypt');
const saltRounds = 10;
// Here we import our Logger file and instantiate a logger object
var logger = require('../logger').Logger;
/*=========================================================
=            Block for the Modals Declarations            =
=========================================================*/
var Login = require('../../db/models').login;
var Person = require('../../db/models').person;
var Studio = require('../../db/models').studio;
var BlackListTokens = require('../../db/models').blak_list_tokens;
var Address = require('../../db/models').address;
const upload = require('../services/upload_files');
const uploadRecord = upload.fields([{
    name: 'logo',
}, {
    name: 'banner',
}]);
const bannerUpload = upload.single('banner');
/*=====  End of Block for the Modals Declarations  ======*/
/**
 *
 * Send One Time Password Code to Mobile Number Given for Varification of account
 *
 */
exports.SendOTPCode = function(req, res) {
    // 1) Check the Validation for request first
    req.checkBody("mobile_number", "Mobile Number is required").notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.send({
            "errors": errors,
            "status": 'error',
            "code": 199
        });
    } else {
        console.log(req.body);
        // 2) Check if Mobile Number Already Exist in the system for code Verification
        var sql1 = 'SELECT * FROM contact_info WHERE mobile_number = ?';
        db.query(sql1, [req.body.mobile_number], function(result, err) {
            if (err) throw err;
            var code = generalService.GetOTPCode();
            if (result.length > 0) {
                logger.info("Comming Here");
                var updateQuery = "UPDATE contact_info SET otp_code = ? WHERE mobile_number = ?";
                var values = [code, req.body.mobile_number];
                db.query(updateQuery, values, function(result, err) {
                    if (err) throw err;
                    // Send OTP Code to the given Mobile Number
                    res.json({
                        "otp_code": code,
                        "status": 'success'
                    });
                });
                // Update The OTP Code with that mobile number and Just Login
            } else {
                // Entry of Mobile Number and OTP Code and send it to the System
                var sql2 = "INSERT INTO contact_info (mobile_number, otp_code, created_at, updatedAt) VALUES ?";
                var values2 = [
                    [req.body.mobile_number, code, new Date(), new Date()]
                ];
                db.query(sql2, [values2], function(result, err) {
                    if (err) throw err;
                    // Send OTP Code to the given Mobile Number
                    res.json({
                        "otp_code": code,
                        "status": 'success'
                    });
                });
            }
        });
    }
};
/**
 *
 * Varify OTP Code that is send to given Mobile number
 *
 */
exports.VarifyOtpCode = function(req, res) {
    req.checkBody("mobile_number", "Mobile Number is required").notEmpty();
    req.checkBody("otp_code", "OTP Code is required").notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.send({
            "errors": errors,
            "status": 'error',
            "code": 199
        });
    } else {
        var sql1 = 'SELECT * FROM contact_info WHERE mobile_number = ? AND otp_code = ?';
        db.query(sql1, [req.body.mobile_number, req.body.otp_code], function(result, err) {
            if (err) throw err;
            if (result.length > 0) {
                var sql2 = 'SELECT * FROM users WHERE contact_id = ? ';
                db.query(sql2, [result[0].id], function(resultRecord, err) {
                    // Update the Flag in Database for the OTP code is verified
                    var updateQuery = "UPDATE contact_info SET verify_code = ? WHERE mobile_number = ?";
                    var values = [1, req.body.mobile_number];
                    db.query(updateQuery, values, function(result, err) {
                        if (err) throw err;
                        if (resultRecord.length > 0) {
                            // Send JWT token with user info
                            var responseData = {
                                // "authorizationCode": generalService.JwtHeader(result[0].id),
                                "data": resultRecord,
                                "status": 'success',
                                "code": 1
                            };
                            res.json(responseData);
                        } else {
                            // New user send to Registration Page
                            var responseData = {
                                "status": 'success',
                                "code": 2
                            };
                            res.json(responseData);
                        }
                    });
                });
            } else {
                res.send({
                    "errors": 'Code does not Match',
                    "status": 'error'
                });
            }
        });
    }
};
/**
 *
 * Registration Process
 *
 */
exports.Registration = function(req, res) {
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    if (req.validationErrors(req.checkBody("country_iso_code").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(201, 'country_iso_code'));
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    if (req.validationErrors(req.checkBody("account_type").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(203, 'account_type'));
    if (req.validationErrors(req.checkBody("first_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("last_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(205, 'last_name'));
    if (req.validationErrors(req.checkBody("email").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(206, 'email'));
    if (req.validationErrors(req.checkBody("email").isEmail()) != false) return res.send(generalService.ChangeValidationErrorObject(207, 'email'));
    if (req.validationErrors(req.checkBody("password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(208, 'password'));
    if (req.validationErrors(req.checkBody("gender").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(210, 'gender'));
    if (req.validationErrors(req.checkBody("dob").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(221, 'dob'));
    if (req.body.account_type == 1) {
        if (req.validationErrors(req.checkBody("studio_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(211, 'studio_name'));
    }
    if (generalService.CheckAge(req.body.dob)) {
        return res.json({
            "code": 222,
            "status": 0,
        });
    }
    var bannerUrl = null;
    var logoUrl = null;
    var sql1 = 'SELECT * FROM logins WHERE mobile_number = ?';
    db.query(sql1, [req.body.mobile_number, req.body.email], function(sql1Result, err1) {
        if (sql1Result.length > 0) {
            // Already Record Existed
            var responseData = {
                "status": 0,
                "code": 212,
                "message": "Mobile Number already Existed"
            };
            return res.json(responseData);
        } else {
            // Check if Email Address is already existed
            var sql10 = 'SELECT * FROM logins WHERE email = ?';
            db.query(sql10, [req.body.email], function(sql10Result, err10) {
                if (sql10Result.length > 0) {
                    var responseData = {
                        "status": 0,
                        "code": 224,
                        "message": "Email Address already Existed"
                    };
                    return res.json(responseData);
                } else {
                    // 1) Add Address Detail Here First
                    var sql2 = "INSERT INTO addresses (line_1, line_2, postcode, country, createdAt, updatedAt) VALUES ?";
                    var values2 = [
                        [req.body.address_line1 != undefined ? req.body.address_line1 : null, req.body.address_line2 != undefined ? req.body.address_line2 : null, req.body.postcode, req.body.country_iso_code, new Date(), new Date()]
                    ];
                    db.query(sql2, [values2], function(sql2Result, err2) {
                        if (err2) return res.json(err2);
                        // Get Address ID and add Person Record
                        var sql4 = "INSERT INTO people (first_name, last_name, gender, dob, can_login, address, createdAt, updatedAt) VALUES ?";
                        var values4 = [
                            [req.body.first_name, req.body.last_name, req.body.gender, req.body.dob, 1, sql2Result.insertId, new Date(), new Date()]
                        ];
                        db.query(sql4, [values4], function(sql4Result, err4) {
                            if (err4) return res.json(err4);
                            // Get Person ID and add Login Record
                            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                                if (err) return res.json(err)
                                var sql5 = "INSERT INTO logins (mobile_number, password, person, email, createdAt, updatedAt, mobile_number_country_iso) VALUES ?";
                                var values5 = [
                                    [req.body.mobile_number, hash, sql4Result.insertId, req.body.email, new Date(), new Date(), req.body.mobile_country_iso]
                                ];
                                db.query(sql5, [values5], function(sql5Result, err5) {
                                    if (err5) return res.json(err5);
                                    console.log(req.body.account_type);
                                    if (req.body.account_type == 1) {
                                        // uploadRecord(req, res, function(err) {
                                        /**
                                         *
                                         * Check if the logo and banner are not empty. 
                                         *
                                         */
                                        // if (err) {
                                        //     return res.json(err);
                                        // } else {
                                        var sql6 = "INSERT INTO studios (name, owner, type, studio_logo, studio_banner, createdAt, updatedAt) VALUES ?";
                                        var values6 = [
                                            // req.files.banner != undefined ? req.files.banner[0].location : 
                                            [req.body.studio_name, sql4Result.insertId, 1, null, null, new Date(), new Date()]
                                        ];
                                        db.query(sql6, [values6], function(sql6Result, err6) {
                                            if (err6) res.send(err6);
                                            // Login Details
                                            login(req.body.mobile_number, req.body.password, req.body.mobile_country_iso, function(login_details) {
                                                return res.json(login_details);
                                            });
                                        });
                                        // }
                                        // })
                                    } else {
                                        login(req.body.mobile_number, req.body.password, req.body.mobile_country_iso, function(login_details) {
                                            return res.json(login_details);
                                        });
                                    }
                                });
                            });
                        });
                    });
                }
            });
        }
    });
};
/**
 *
 * Login Process
 *
 */
exports.Login = function(req, res) {
    // Added Comments
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    if (req.validationErrors(req.checkBody("password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(208, 'password'));
    login(req.body.mobile_number, req.body.password, req.body.mobile_country_iso, function(login_details) {
        return res.json(login_details);
    });
};
/**
 *
 * Check Authentication
 *
 */
exports.loginRequired = function(req, res, next) {
    if (req.headers['authorization'] != undefined) {
        var sql0 = 'SELECT * FROM blak_list_tokens WHERE token = ?';
        db.query(sql0, [req.headers['authorization']], function(sql0Result, err0) {
            if (err0) return res.json(err0)
            if (sql0Result.length > 0) {
                return res.json({
                    status: 0,
                    message: "Unauthorized User",
                    code: 401
                });
            } else {
                if (req.body.user_id != undefined) {
                    generalService.CheckUserExistWithToken(req.body.user_id, req.headers['authorization'], function(response) {
                        if (response) {
                            next()
                        } else {
                            return res.json({
                                status: 0,
                                message: "Unauthorized User",
                                code: 401
                            });
                        }
                    })
                } else {
                    generalService.JwtHeaderResponse(req.headers['authorization'], function(responseRecord) {
                        if (responseRecord) {
                            var sql1 = 'SELECT * FROM logins WHERE id = ?';
                            db.query(sql1, [responseRecord.userId], function(sql1Result, err1) {
                                if (sql1Result.length > 0) {
                                    next()
                                } else {
                                    return res.json({
                                        status: 0,
                                        message: "Unauthorized User",
                                        code: 401
                                    });
                                }
                            });
                        } else {
                            return res.json({
                                status: 0,
                                message: "Unauthorized User",
                                code: 401
                            });
                        }
                    });
                }
            }
        });
    } else {
        return res.json({
            status: 0,
            message: "Unauthorized User",
            code: 401
        });
    }
}
exports.verifyToken = function(req, res) {
    generalService.JwtHeaderResponse(req.headers['authorization'], function(responseRecord) {
        userDetails(responseRecord.userId, req.headers['authorization'], function(response) {
            return res.json(response);
        });
    });
}
/**
 *
 * Function for Login System 
 *
 */
function login(mobile_number, password, mobile_country_iso, callback) {
    var sql1 = 'SELECT s.id as studio_id, l.createdAt, l.mobile_number, l.password, l.id as user_id, l.email, l.mobile_number_country_iso,p.id as person_id, p.first_name, p.last_name, p.gender, p.dob, a.line_1, a.line_2, a.city, a.postcode, a.province, a.country FROM logins l LEFT JOIN people p ON l.person = p.id LEFT JOIN addresses a ON p.address = a.id LEFT JOIN studios s ON p.id = s.owner WHERE l.mobile_number = ? AND l.mobile_number_country_iso = ?';
    db.query(sql1, [mobile_number, mobile_country_iso], function(sql1Result, err1) {
        if (err1) throw err1;
        if (sql1Result.length > 0) {
            bcrypt.compare(password, sql1Result[0].password, function(err, res) {
                if (res) {
                    var studioRecord = null
                    if (sql1Result[0].studio_id) {
                        studioRecord = sql1Result[0].studio_id
                    }
                    var dateConversion = new Date(sql1Result[0].dob * 1000);
                    var responseData = {
                        "user": {
                            "user_id": sql1Result[0].user_id,
                            "first_name": sql1Result[0].first_name,
                            "last_name": sql1Result[0].last_name,
                            "email": sql1Result[0].email,
                            "mobile_country_iso": sql1Result[0].mobile_number_country_iso,
                            "mobile_number": sql1Result[0].mobile_number,
                            "personal": {
                                "id": sql1Result[0].person_id,
                                "dob": sql1Result[0].dob,
                                "dob_in_string_date": dateConversion.getDate() + "/" + (dateConversion.getMonth() + 1) + "/" + dateConversion.getFullYear(),
                                "gender": sql1Result[0].gender,
                                "address_line1": sql1Result[0].line_1,
                                "address_line2": sql1Result[0].line_2,
                                "postcode": sql1Result[0].postcode,
                                "country": sql1Result[0].country,
                                "createdAt": generalService.GetRelatedDateTime(sql1Result[0].createdAt, "Asia/Karachi")
                            },
                            "studio": studioRecord,
                        },
                        "userToken": generalService.JwtHeader(sql1Result[0].user_id, sql1Result[0].person_id, studioRecord),
                        "status": 1,
                        "message": 'success',
                        "code": 200
                    };
                    return callback(responseData);
                } else {
                    var responseData = {
                        "status": 0,
                        "code": 213,
                        "message": "Invalid mobile number or password, please retry"
                    };
                    return callback(responseData);
                }
            });
        } else {
            var responseData = {
                "status": 0,
                "code": 214,
                "message": "Account does not exist"
            };
            return callback(responseData);
        }
    });
}
/**
 *
 * Function for Check Mobile Number
 *
 */
exports.CheckMobileNumber = function(req, res) {
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    var sql1 = 'SELECT id FROM logins WHERE mobile_number = ? AND mobile_number_country_iso = ?';
    db.query(sql1, [req.body.mobile_number, req.body.mobile_country_iso], function(sql1Result, err1) {
        console.log(sql1Result);
        if (sql1Result.length > 0) {
            return res.status(200).json({
                status: 1,
                message: "success",
                code: 200
            });
        } else {
            return res.json({
                status: 0,
                message: "Mobile Number not found",
                code: 215
            });
        }
    });
}
/**
 *
 * Function for Getting User Information
 *
 */
exports.GetUserDetails = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    userDetails(req.body.user_id, req.headers['authorization'], function(response) {
        return res.json(response);
    });
}
/**
 *
 * Function for Logout User
 *
 */
exports.Logout = function(req, res) {
    generalService.JwtHeaderResponse(req.headers['authorization'], function(responseRecord) {
        var sql1 = 'SELECT * FROM blak_list_tokens WHERE user_id = ?';
        db.query(sql1, [responseRecord.userId], function(sql1Result, err1) {
            if (sql1Result.length > 0) {
                var sql2 = "UPDATE blak_list_tokens SET token = ? WHERE user_id = ?";
            } else {
                var sql2 = "INSERT INTO blak_list_tokens (token, user_id, createdAt, updatedAt) VALUES (?, ?, ?, ?)"
            }
            var values2 = [req.headers['authorization'], responseRecord.userId, new Date(), new Date()];
            db.query(sql2, values2, function(sql2Result, err2) {
                if (err2) return res.json(err2);
                var responseData = {
                    "user": {
                        "userToken": {
                            "Authorization": null,
                        },
                    },
                    "status": 1,
                    "message": 'success',
                    "code": 200
                }
                return res.json(responseData);
            });
        });
    });
}
/**
 *
 * Function for Changing Password
 *
 */
exports.ChangePassword = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("old_password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(218, 'old_password'));
    if (req.validationErrors(req.checkBody("new_password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(219, 'new_password'));
    var sql1 = 'SELECT l.mobile_number, l.password, l.id as user_id, l.email, p.first_name, p.last_name, p.gender, p.dob, a.line_1, a.line_2, a.city, a.postcode, a.province, a.country FROM logins l, addresses a, people p WHERE p.address = a.id AND l.person = p.id AND l.id = ?';
    db.query(sql1, [req.body.user_id], function(sql1Result, err1) {
        if (err1) return err1;
        if (sql1Result.length > 0) {
            bcrypt.compare(req.body.old_password, sql1Result[0].password, function(err2, resData) {
                if (err2) return err2;
                if (resData) {
                    bcrypt.hash(req.body.new_password, saltRounds, function(err3, hash) {
                        if (err3) return err3;
                        var updateQuery = "UPDATE logins SET password = ? WHERE id = ?";
                        var values = [hash, req.body.user_id];
                        db.query(updateQuery, values, function(result, err) {
                            if (err) throw err;
                            // Return Success Message
                            return res.json({
                                "status": 1,
                                "message": 'success',
                                "code": 200
                            });
                        });
                    });
                } else {
                    var responseData = {
                        "status": 0,
                        "code": 217,
                        "message": "Invalid old password, please retry"
                    };
                    return res.json(responseData);
                }
            });
        }
    })
}
/**
 *
 * Update User Profile Information
 *
 */
exports.UpdateProfile = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    if (req.validationErrors(req.checkBody("country_iso_code").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(201, 'country_iso_code'));
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    if (req.validationErrors(req.checkBody("first_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("last_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(205, 'last_name'));
    if (req.validationErrors(req.checkBody("email").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(206, 'email'));
    if (req.validationErrors(req.checkBody("email").isEmail()) != false) return res.send(generalService.ChangeValidationErrorObject(207, 'email'));
    if (req.validationErrors(req.checkBody("gender").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(210, 'gender'));
    if (req.validationErrors(req.checkBody("dob").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(221, 'dob'));
    if (generalService.CheckAge(req.body.dob)) {
        return res.json({
            "code": 222,
            "status": 0,
        });
    }
    generalService.UserExist(req.body.user_id, function(response) {
        if (response) {
            // Check if the Email or Mobile Number is existed already with other account
            var sql0 = 'SELECT * FROM logins WHERE mobile_number = ? AND id != ?';
            db.query(sql0, [req.body.mobile_number, req.body.user_id], function(sql0Result, err0) {
                if (err0) return res.json(err0);
                if (sql0Result.length > 0) {
                    return res.json({
                        "status": 0,
                        "code": 212,
                        "message": "Mobile Number already Existed"
                    });
                } else {
                    // Check if Email Address is already existed
                    var sql10 = 'SELECT * FROM logins WHERE email = ? AND id != ?';
                    db.query(sql10, [req.body.email, req.body.user_id], function(sql10Result, err10) {
                        if (sql10Result.length > 0) {
                            var responseData = {
                                "status": 0,
                                "code": 224,
                                "message": "Email Address already Existed"
                            };
                            return res.json(responseData);
                        } else {
                            var sql1 = 'SELECT a.id as address_id, l.person as person_id FROM logins l, people p, addresses a WHERE l.person = p.id AND p.address = a.id AND l.id = ?';
                            db.query(sql1, [req.body.user_id], function(sql1Result, err1) {
                                if (sql1Result.length > 0) {
                                    // 1) Update Address Detail Here First
                                    var sql2 = "UPDATE addresses SET line_1 = ?, line_2 = ?, postcode = ?, country = ?, updatedAt = ? WHERE id = ?";
                                    var values2 = [
                                        req.body.address_line1 != undefined ? req.body.address_line1 : null, req.body.address_line2 != undefined ? req.body.address_line2 : null, req.body.postcode, req.body.country_iso_code, new Date(), sql1Result[0].address_id
                                    ];
                                    db.query(sql2, values2, function(sql2Result, err2) {
                                        if (err2) return res.json(err2);
                                        // Update Person Record
                                        var sql4 = "UPDATE people SET first_name = ?, last_name = ?, gender = ?, dob = ?, can_login = ?, updatedAt = ?  WHERE id = ?";
                                        var values4 = [req.body.first_name, req.body.last_name, req.body.gender, req.body.dob, 1, new Date(), sql1Result[0].person_id];
                                        db.query(sql4, values4, function(sql4Result, err4) {
                                            if (err4) return res.json(err4);
                                            // Update Login Record
                                            var sql5 = "UPDATE logins SET mobile_number = ?, email = ?, updatedAt = ?, mobile_number_country_iso = ? WHERE id = ?";
                                            var values5 = [
                                                req.body.mobile_number, req.body.email, new Date(), req.body.mobile_country_iso, req.body.user_id
                                            ];
                                            db.query(sql5, values5, function(sql5Result, err5) {
                                                if (err5) return res.json(err5);
                                                userDetails(req.body.user_id, req.headers['authorization'], function(response) {
                                                    return res.json(response);
                                                });
                                            });
                                        });
                                    });
                                } else {
                                    // Record Does Not Exist
                                    var responseData = {
                                        "status": 0,
                                        "code": 223,
                                        "message": "Record not found"
                                    };
                                    return res.json(responseData);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            var responseData = {
                "status": 0,
                "code": 214,
                "message": "Account does not exist"
            };
            return res.json(responseData);
        }
    });
}
/**
 *
 * Reset Password
 *
 */
exports.ResetPassword = function(req, res) {
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    if (req.validationErrors(req.checkBody("new_password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(208, 'new_Password'));
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    var sql1 = 'SELECT id FROM logins WHERE mobile_number = ? AND mobile_number_country_iso = ?';
    db.query(sql1, [req.body.mobile_number, req.body.mobile_country_iso], function(sql1Result, err1) {
        if (sql1Result.length > 0) {
            // Update New Password
            bcrypt.hash(req.body.new_password, saltRounds, function(err, hash) {
                var sql2 = "UPDATE logins SET password = ? WHERE id = ?";
                var values2 = [
                    hash, sql1Result[0].id
                ];
                db.query(sql2, values2, function(sql2Result, err2) {
                    if (err2) return res.json(err2);
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200
                    });
                });
            });
        } else {
            var responseData = {
                "status": 0,
                "code": 223,
                "message": "Record not found"
            };
            return res.json(responseData);
        }
    });
}
/**
 *
 * Check if User Register for Studio
 *
 */
exports.CheckUserRegisterForStudio = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
}
/**
 *
 * Function for User Details
 *
 */
function userDetails(user_id, token, callback) {
    // var sql1 = 'SELECT l.createdAt, l.mobile_number, l.password, l.id as user_id, l.email, l.mobile_number_country_iso,p.id as person_id, p.first_name, p.last_name, p.gender, p.dob, a.line_1, a.line_2, a.city, a.postcode, a.province, a.country FROM logins l LEFT JOIN people p ON l.person = p.id LEFT JOIN addresses a ON p.address = a.id WHERE l.id = ?';
    var sql1 = 'SELECT s.id as studio_id, l.createdAt, l.mobile_number, l.password, l.id as user_id, l.email, l.mobile_number_country_iso,p.id as person_id, p.first_name, p.last_name, p.gender, p.dob, a.line_1, a.line_2, a.city, a.postcode, a.province, a.country FROM logins l LEFT JOIN people p ON l.person = p.id LEFT JOIN addresses a ON p.address = a.id LEFT JOIN studios s ON p.id = s.owner WHERE l.id = ?';
    db.query(sql1, [user_id], function(sql1Result, err1) {
        if (sql1Result.length > 0) {
            var dateConversion = new Date(sql1Result[0].dob * 1000);
            var studioRecord = null
            if (sql1Result[0].studio_id) {
                studioRecord = sql1Result[0].studio_id
            }
            var responseData = {
                "user": {
                    "user_id": sql1Result[0].user_id,
                    "first_name": sql1Result[0].first_name,
                    "last_name": sql1Result[0].last_name,
                    "email": sql1Result[0].email,
                    "mobile_country_iso": sql1Result[0].mobile_number_country_iso,
                    "mobile_number": sql1Result[0].mobile_number,
                    "personal": {
                        "id": sql1Result[0].person_id,
                        "dob": sql1Result[0].dob,
                        "dob_in_string_date": dateConversion.getDate() + "/" + (dateConversion.getMonth() + 1) + "/" + dateConversion.getFullYear(),
                        "gender": sql1Result[0].gender,
                        "address_line1": sql1Result[0].line_1,
                        "address_line2": sql1Result[0].line_2,
                        "postcode": sql1Result[0].postcode,
                        "country": sql1Result[0].country,
                        "createdAt": generalService.GetRelatedDateTime(sql1Result[0].createdAt)
                    },
                    "studio": studioRecord,
                },
                "userToken": token,
                "status": 1,
                "message": 'success',
                "code": 200
            };
            callback(responseData);
        } else {
            var responseData = {
                "status": 0,
                "code": 214,
                "message": "Account does not exist"
            };
            callback(responseData);
        }
    });
}
/**
 *
 * Testing ORM Structure first
 *
 */
exports.CheckTestingLogin = function(req, res) {
    if (req.validationErrors(req.checkBody("mobile_country_iso").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(220, 'mobile_country_iso'));
    if (req.validationErrors(req.checkBody("country_iso_code").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(201, 'country_iso_code'));
    if (req.validationErrors(req.checkBody("mobile_number").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(202, 'mobile_number'));
    if (req.validationErrors(req.checkBody("mobile_number").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(279, 'mobile_number'));
    if (req.validationErrors(req.checkBody("account_type").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(203, 'account_type'));
    if (req.validationErrors(req.checkBody("first_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("last_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(205, 'last_name'));
    if (req.validationErrors(req.checkBody("email").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(206, 'email'));
    if (req.validationErrors(req.checkBody("email").isEmail()) != false) return res.send(generalService.ChangeValidationErrorObject(207, 'email'));
    if (req.validationErrors(req.checkBody("password").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(208, 'password'));
    if (req.validationErrors(req.checkBody("gender").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(210, 'gender'));
    if (req.validationErrors(req.checkBody("dob").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(221, 'dob'));
    if (req.body.account_type == 1) {
        if (req.validationErrors(req.checkBody("studio_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(211, 'studio_name'));
    }
    if (generalService.CheckAge(req.body.dob)) {
        return res.json({
            "code": 222,
            "status": 0,
        });
    }
    // ORM Implementation
    // 1) Check If Mobile Number is Already Existed
    Login.findAll({
        where: {
            mobile_number: req.body.mobile_number
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            var responseData = {
                "status": 0,
                "code": 212,
                "message": "Mobile Number already Existed"
            };
            return res.json(responseData);
        } else {
            // 2) Check If Email Address is Already Existed
            Login.findAll({
                where: {
                    email: req.body.email
                }
            }).then(sql2Result => {
                if (sql2Result.length > 0) {
                    var responseData = {
                        "status": 0,
                        "code": 224,
                        "message": "Email Address already Existed"
                    };
                    return res.json(responseData);
                } else {
                    // 3) Add Address of Person Record First
                    Address.create({
                        line_1: req.body.address_line1 != undefined ? req.body.address_line1 : null,
                        line_2: req.body.address_line2 != undefined ? req.body.address_line2 : null,
                        postcode: req.body.postcode,
                        country: req.body.country_iso_code,
                    }).then(sql3Result => {
                        // 4) Add Record in Person Table with Foreign key of Address record
                        Person.create({
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            gender: req.body.gender,
                            dob: req.body.dob,
                            can_login: 1,
                            address: sql3Result.id
                        }).then(sql4Result => {
                            // 4) Add Record in Login Table with Foreign key of Person record
                            Login.create({
                                mobile_number: req.body.mobile_number,
                                password: req.body.password,
                                person: sql4Result.id,
                                email: req.body.email
                            }).then(sql5Result => {
                                if (req.body.account_type == 1) {
                                    Studio.create({
                                        name: req.body.studio_name,
                                        owner: sql4Result.id,
                                        type: 1,
                                        studio_logo: req.body.logo_url != undefined ? req.body.logo_url : null,
                                        studio_banner: req.body.banner_url != undefined ? req.body.banner_url : null
                                    }).then(sql6Result => {
                                        return res.json({
                                            "status": 'success with studio'
                                        });
                                    }).catch(function(err6) {
                                        if (err6) return res.json(err6)
                                    })
                                } else {
                                    return res.json({
                                        "status": 'success without studio'
                                    });
                                }
                            }).catch(function(err5) {
                                if (err5) return res.json(err5)
                            })
                        }).catch(function(err4) {
                            if (err4) return res.json(err4)
                        })
                    }).catch(function(err3) {
                        if (err3) return res.json(err3)
                    })
                }
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Upload File object to S3 bucket
 *
 */
function uploadFile(fileObject, callBack) {
    singleUpload(req, res, function(err) {
        if (err) {
            callBack({
                'status': 0,
                'title': 'File Upload Error',
                'message': err.message
            })
        }
        callBack({
            'status': 1,
            'title': 'success',
            'imageUrl': req.file.location
        })
    });
}