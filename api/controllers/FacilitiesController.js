/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var generalService = require('../services/GeneralService.js');
var Person = require('../../db/models').person;
var GuardianStudent = require('../../db/models').guardian_student;
var Address = require('../../db/models').address;
var StudioCenter = require('../../db/models').studio_center;
var Studio = require('../../db/models').studio;
var Facility = require('../../db/models').facility;
/*----------  Implementation of Facility Controller Logic  ----------*/
/**
 *
 * Create Facilities
 *
 */
exports.Create = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("capacity").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(281, 'capacity'));
    if (req.validationErrors(req.checkBody("capacity").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(282, 'capacity'));
    // 1) Check if the Studio Center is Listed in the given Studio and is listed in the the Table as well
    // StudioCenter.findAll({}).
    Facility.findAll({
        where: {
            name: req.body.name,
            studio_center: req.body.studio_center
        }
    }).then(sql0Result => {
        if (sql0Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Facility.create(req.body).then(sql1Result => {
                // 2) Save record in the Facility Table.
                getFacilities(req.body.studio_center, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    })
                })
            }).catch(function(err1) {
                // Through DB related issue Errors
                if (err1) return res.json(err1)
            })
        }
    }).catch(function(err0) {
        // Through DB related issue Errors
        if (err0) return res.json(err2)
    })
}
/**
 *
 * Update Facility
 *
 */
exports.Update = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("capacity").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(281, 'capacity'));
    if (req.validationErrors(req.checkBody("capacity").isNumeric()) != false) return res.send(generalService.ChangeValidationErrorObject(282, 'capacity'));
    if (req.validationErrors(req.checkBody("facility").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(266, 'facility'));
    // 1) Check if the Studio Center exist.
    Facility.findAll({
        where: {
            name: req.body.name,
            studio_center: req.body.studio_center,
            id: {
                $not: req.body.facility
            }
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            // 2) Update Facility Record 
            Facility.update(req.body, {
                where: {
                    id: req.body.facility
                }
            }).then(sql2Result => {
                getFacilities(req.body.studio_center, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    })
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Facility
 *
 */
exports.Delete = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("facility").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(266, 'facility'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    Facility.destroy({
        where: {
            id: req.body.facility
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getFacilities(req.body.studio_center, function(record) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": record
                });
            })
        } else {
            return res.json({
                "code": 278,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Retrieve all Facilities based on Studio Center
 *
 */
exports.FacilityRecord = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    getFacilities(req.body.studio_center, function(responseRecord) {
        if (responseRecord.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": responseRecord
            });
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    })
}
/**
 *
 * Function for Retrieving Facilities on basis of the studio center
 *
 */
function getFacilities(studio_center, callback) {
    Facility.findAll({
        where: {
            studio_center: studio_center
        }
    }).then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}