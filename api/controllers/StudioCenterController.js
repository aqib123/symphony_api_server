/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var generalService = require('../services/GeneralService.js');
var Person = require('../../db/models').person;
var GuardianStudent = require('../../db/models').guardian_student;
var Address = require('../../db/models').address;
var StudioCenter = require('../../db/models').studio_center;
var Studio = require('../../db/models').studio;
var Facility = require('../../db/models').facility;
/*----------  Implementation of Studio Center Controller Logic  ----------*/
/**
 *
 * Create Studio Center
 *
 */
exports.Create = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(292, 'name'));
    if (req.validationErrors(req.checkBody("country").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(201, 'country_iso_code'));
    if (req.validationErrors(req.checkBody("postcode").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(209, 'postcode'));
    if (req.validationErrors(req.checkBody("line_1").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(284, 'line_1'));
    // 1) Save Record Into Person Table First
    StudioCenter.findAll({
        where: {
            name: req.body.name,
            studio: req.body.studio
        }
    }).then(sql0Result => {
        if (sql0Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            Address.create(req.body).then(sql1Result => {
                // 2) Save record in Address.
                StudioCenter.create({
                    studio: req.body.studio,
                    address: sql1Result.id,
                    name: req.body.name
                }).then(sql2Result => {
                    getStudioCenter(req.body.studio, function(responseRecord) {
                        return res.json({
                            "status": 1,
                            "message": 'success',
                            "code": 200,
                            "record": responseRecord
                        });
                    })
                }).catch(function(err2) {
                    // Through DB related issue Errors
                    if (err2) return res.json(err2)
                })
            }).catch(function(err1) {
                // Through DB related issue Errors
                if (err1) return res.json(err1)
            })
        }
    }).catch(function(err0) {
        // Through DB related issue Errors
        if (err0) return res.json(err2)
    })
}
/**
 *
 * Update Studio Center
 *
 */
exports.Update = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    if (req.validationErrors(req.checkBody("name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(292, 'name'));
    if (req.validationErrors(req.checkBody("country").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(201, 'country_iso_code'));
    if (req.validationErrors(req.checkBody("postcode").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(209, 'postcode'));
    if (req.validationErrors(req.checkBody("line_1").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(284, 'line_1'));
    // 1) Check if the Studio Center exist.
    StudioCenter.findAll({
        where: {
            name: req.body.name,
            studio: req.body.studio,
            id: {
                $not: req.body.studio_center
            }
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "code": 278,
                "status": 0,
            });
        } else {
            // 2) Update Studio center record
            StudioCenter.update(req.body, {
                where: {
                    id: req.body.studio_center
                }
            }).then(sql2Result => {
                StudioCenter.findAll({
                    where: {
                        id: req.body.studio_center
                    }
                }).then(sql4Result => {
                    if (sql4Result.length > 0) {
                        // Update Address Record as well
                        Address.update(req.body, {
                            where: {
                                id: sql4Result[0].address
                            }
                        }).then(sql3Result => {
                            getStudioCenter(req.body.studio, function(responseRecord) {
                                return res.json({
                                    "status": 1,
                                    "message": 'success',
                                    "code": 200,
                                    "record": responseRecord
                                });
                            })
                        }).catch(function(err3) {
                            if (err3) return res.json(err3)
                        })
                    }
                }).catch(function(err4) {
                    if (err4) return res.json(err4)
                })
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Studio Center
 *
 */
exports.Delete = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio_center").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(280, 'studio_center'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    StudioCenter.destroy({
        where: {
            id: req.body.studio_center
        }
    }).then(sql1Result => {
        if (sql1Result != 0) {
            getStudioCenter(req.body.studio, function(record) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": record
                });
            })
        } else {
            return res.json({
                "code": 278,
                "status": 0,
            });
        }
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json({
            "code": 249,
            "status": 0,
        });
    })
}
/**
 *
 * Retrieve all Studios Center on basis of studio
 *
 */
exports.StudioCenterRecord = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    getStudioCenter(req.body.studio, function(responseRecord) {
        if (responseRecord.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": responseRecord
            });
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    })
}
/**
 *
 * Function for Retrieving Studio Center on basis of studio
 *
 */
function getStudioCenter(studio, callback) {
    // Get Active Student Record
    StudioCenter.findAll({
        where: {
            studio: studio
        },
        include: [{
            model: Studio,
            as: 'studioInformation',
            all: true,
        }, {
            model: Address,
            as: 'addressInformation',
            all: true,
        }, {
            model: Facility,
            as: 'facilitiesInformation',
            all: true,
        }],
    }).then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}