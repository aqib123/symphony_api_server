/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var generalService = require('../services/GeneralService.js');
var Person = require('../../db/models').person;
var GuardianStudent = require('../../db/models').guardian_student;
var CourseEnrollment = require('../../db/models').course_enrolment;
/*----------  Implementation of Student Controller Logic  ----------*/
/**
 *
 * Create Student
 *
 */
exports.Create = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    if (req.validationErrors(req.checkBody("first_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("last_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(205, 'last_name'));
    if (req.validationErrors(req.checkBody("gender").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(210, 'gender'));
    if (req.validationErrors(req.checkBody("dob").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(221, 'dob'));
    // if (generalService.CheckAge(req.body.dob)) {
    //     return res.json({
    //         "code": 222,
    //         "status": 0,
    //     });
    // }
    /**
        TODO:
        - Need to clarify how to check if student already exist.
     */
    // 1) Save Record Into Person Table First
    Person.create({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        dob: req.body.dob,
        can_login: 0,
    }).then(sql1Result => {
        // 2) Save record in the Guardian Table. Add Student under one guardian must.
        GuardianStudent.create({
            guardian: req.body.parent_id,
            student: sql1Result.id,
            is_owner: 1
        }).then(sql2Result => {
            getStudentRecord(req.body.parent_id, function(responseRecord) {
                return res.json({
                    "status": 1,
                    "message": 'success',
                    "code": 200,
                    "record": responseRecord
                });
            });
        }).catch(function(err2) {
            // Through DB related issue Errors
            if (err2) return res.json(err2)
        })
    }).catch(function(err1) {
        // Through DB related issue Errors
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Update Student
 *
 */
exports.Update = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("student_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(226, 'student_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    if (req.validationErrors(req.checkBody("first_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(204, 'first_name'));
    if (req.validationErrors(req.checkBody("last_name").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(205, 'last_name'));
    if (req.validationErrors(req.checkBody("gender").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(210, 'gender'));
    if (req.validationErrors(req.checkBody("dob").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(221, 'dob'));
    if (generalService.CheckAge(req.body.dob)) {
        return res.json({
            "code": 222,
            "status": 0,
        });
    }
    // 1) Check if the Stduent is associated with any guardian and also check if the guardian is owner.
    GuardianStudent.findAll({
        where: {
            student: req.body.student_id,
            is_owner: 1,
            guardian: req.body.parent_id,
        }
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            // 2) Update Personal Record of Student.
            Person.update({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                gender: req.body.gender,
                dob: req.body.dob,
                can_login: 0,
            }, {
                where: {
                    id: req.body.student_id
                }
            }).then(sql2Result => {
                getStudentRecord(req.body.parent_id, function(responseRecord) {
                    return res.json({
                        "status": 1,
                        "message": 'success',
                        "code": 200,
                        "record": responseRecord
                    });
                });
            }).catch(function(err2) {
                if (err2) return res.json(err2)
            })
        } else {
            var responseData = {
                "status": 0,
                "code": 227,
                "message": "Student Guardian not exist or guardian is not an owner so he cannot uopdate record."
            };
            return res.json(responseData);
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Delete Student
 *
 */
exports.Delete = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("student_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(226, 'student_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    // Check if the Student is Already enrolled in the course 
    CourseEnrollment.findAll({
        where: {
            student: req.body.student_id,
        }
    }).then(sql0Result => {
        if (sql0Result.length > 0) {
            return res.json({
                "code": 295,
                "status": 0,
            });
        } else {
            GuardianStudent.findAll({
                where: {
                    student: req.body.student_id,
                    is_owner: 1,
                    guardian: req.body.parent_id,
                }
            }).then(sql1Result => {
                if (sql1Result.length > 0) {
                    // Inactive Record
                    Person.update({
                        deletedAt: new Date(),
                    }, {
                        where: {
                            id: req.body.student_id
                        }
                    }).then(sql2Result => {
                        // Delete Guardian Student
                        GuardianStudent.destroy({
                            where: {
                                student: req.body.student_id,
                                is_owner: 1,
                                guardian: req.body.parent_id,
                            }
                        }).then(sql3Result => {
                            getStudentRecord(req.body.parent_id, function(responseRecord) {
                                return res.json({
                                    "status": 1,
                                    "message": 'success',
                                    "code": 200,
                                    "record": responseRecord
                                });
                            });
                        }).catch(function(err3) {
                            if (err3) return res.json(err3)
                        })
                    }).catch(function(err2) {
                        if (err2) return res.json(err2)
                    })
                } else {
                    var responseData = {
                        "status": 0,
                        "code": 227,
                        "message": "Student Guardian not exist or guardian is not an owner so he cannot uopdate record."
                    };
                    return res.json(responseData);
                }
            }).catch(function(err1) {
                if (err1) return res.json(err1)
            })
        }
    }).catch(function(err0) {
        if (err0) return res.json(err0)
    })
}
/**
 *
 * Retrieve all Students
 *
 */
exports.StudentRecord = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    getStudentRecord(req.body.parent_id, function(responseRecord) {
        if (responseRecord.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": responseRecord
            });
        } else {
            return res.json({
                "code": 223,
                "status": 0,
            });
        }
    });
}
/**
 *
 * Function for Retrieving Students of the Guardian
 *
 */
function getStudentRecord(guardianId, callback) {
    // Get Active Student Record
    GuardianStudent.findAll({
        where: {
            guardian: guardianId,
        },
        include: [{
            all: true,
            model: Person
        }, ],
    }).then(sql1Result => {
        callback(sql1Result)
    }).catch(function(err1) {
        if (err1) callback(err1)
    })
}