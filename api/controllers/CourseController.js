/**
 *
 * Dependencies Require
 *
 */
require('dotenv').config();
var request = require('request');
var generalService = require('../services/GeneralService.js');
var Person = require('../../db/models').person;
var Course = require('../../db/models').course;
var CourseEnrollment = require('../../db/models').course_enrolment;
var Transaction = require('../../db/models').transaction;
var Person = require('../../db/models').person;
var Login = require('../../db/models').login;
var GuardianStudent = require('../../db/models').guardian_student;
var Studio = require('../../db/models').studio;
var CourseTemplate = require('../../db/models').course_template;
var Facility = require('../../db/models').facility;
var Category = require('../../db/models').category;
var SubCategory = require('../../db/models').sub_category;
var Currency = require('../../db/models').currency;
var StudioCenter = require('../../db/models').studio_center;
var Address = require('../../db/models').address;
var Class = require('../../db/models').class;
var StudioCenter = require('../../db/models').studio_center;
/*----------  Implementation of Course Controller Logic  ----------*/
/**
 *
 * Enroll Course to For Students
 *
 */
exports.EnrollCourse = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("course_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(228, 'course_id'));
    // if (req.validationErrors(req.checkBody("student_id").isArray()) != false) return res.send(generalService.ChangeValidationErrorObject(229, 'student_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    if (req.validationErrors(req.checkBody("student_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(226, 'student_id'));
    if (req.validationErrors(req.checkBody("currency").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(230, 'currency'));
    if (req.validationErrors(req.checkBody("amount_paid").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(231, 'amount_paid'));
    // if (req.validationErrors(req.checkBody("paid_at").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(235, 'paid_at'));
    // 1) Validation Requirement
    /*----------  check if all the students lie under parent or not  ----------*/
    generalService.CheckParentStudents(req.body.parent_id, req.body.student_id, function(response) {
        if (response.length > 0) {
            var responseData = {
                "status": 0,
                "code": 232,
                "students_id": response
            };
            return res.json(responseData)
        } else {
            var count = 1;
            // Check if the Students are already register with given course id
            generalService.CheckStudentExistWithCourse(req.body.course_id, req.body.student_id, function(responseRecord) {
                if (responseRecord.length > 0) {
                    var responseData = {
                        "status": 0,
                        "code": 233,
                        "students_id": responseRecord
                    };
                    return res.json(responseData)
                } else {
                    // First Check if the Currency Already Exist in the System
                    generalService.CheckCurrencyExist(req.body.currency, function(responseRecord2) {
                        if (responseRecord2) {
                            var courseEnrollmentArray = [];
                            var transactionArray = [];
                            req.body.student_id.forEach(function(record) {
                                // Create Array of Object to be saved in the array for bulk insert
                                courseEnrollmentArray.push({
                                    course_id: req.body.course_id,
                                    student: record,
                                    paid_amount: req.body.amount_paid,
                                    paid_at: new Date()
                                });
                                // Add Objects into Transaction Array
                                transactionArray.push({
                                    time: new Date(),
                                    madeby: req.body.parent_id,
                                    description: 'Enrollment of Students in Given Course',
                                    currency: req.body.currency,
                                    amount: req.body.amount_paid,
                                    course: req.body.course_id,
                                    student: record
                                });
                            })
                            // // Save Record For the Course Enrollment
                            CourseEnrollment.bulkCreate(courseEnrollmentArray).then(sql2Result => {
                                // Save record in Transaction Table
                                Transaction.bulkCreate(transactionArray).then(sql3Result => {
                                    return res.json({
                                        "status": 1,
                                        "message": 'success',
                                        "code": 200,
                                        "record": {
                                            "transactionRecord": sql3Result,
                                            "courseId": req.body.course_id
                                        }
                                    });
                                }).catch(function(err3) {
                                    if (err3) return res.json(err3)
                                })
                            }).catch(function(err2) {
                                // Through DB related issue Errors
                                if (err2) return res.json(err2)
                            })
                        } else {
                            var responseData = {
                                "status": 0,
                                "code": 234
                            };
                            return res.json(responseData)
                        }
                    })
                }
            })
        }
    });
}
/**
 *
 * Get All the Courses of Students
 *
 */
exports.GetAllCoursesOfStudents = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("parent_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(225, 'parent_id'));
    // Get Active Student Record
    GuardianStudent.findAll({
        where: {
            guardian: req.body.parent_id,
        },
        include: [{
            model: Person,
            as: 'studentPersonalRecord',
            include: [{
                model: CourseEnrollment,
                as: 'courses',
                include: [{
                    model: Course,
                    as: 'courseDetails',
                    include: [{
                        model: CourseTemplate,
                        as: 'courseTemplateInformation',
                        include: [{
                            model: Category,
                            as: 'categoryInformation',
                        }, {
                            model: SubCategory,
                            as: 'subCategoryInformation',
                        }, {
                            model: Currency,
                            as: 'currencyInformation',
                        }],
                    }, {
                        model: Facility,
                        as: 'facilityInformation',
                        include: [{
                            model: StudioCenter,
                            as: 'studioCenterRecord',
                            include: [{
                                model: Address,
                                as: 'addressInformation',
                            }]
                        }]
                    }, {
                        model: Studio,
                        as: 'studioInformation',
                    }, {
                        model: Class,
                        as: 'classRecord'
                    }]
                }]
            }]
        }]
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            });
        } else {
            return res.json({
                "status": 0,
                "code": 223
            });
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Get All Courses
 *
 */
exports.GetAllCourses = function(req, res) {
    if (req.validationErrors(req.checkBody("user_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(216, 'user_id'));
    if (req.validationErrors(req.checkBody("studio").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(240, 'studio'));
    var orderBy = 'ASC';
    // return res.json(whereObject)
    if (req.body.order_by != undefined) {
        orderBy = req.body.order_by
        if (orderBy != 'ASC' || orderBy != 'DESC') {
            orderBy = 'ASC'
        }
    }
    Studio.findAll({
        where: {
            id: req.body.studio
        },
        order: [
            ['createdAt', orderBy],
        ],
        include: [{
            // all: true,
            model: CourseTemplate,
            // nested: true,
            as: 'courseTemplateRecord',
            include: [{
                // all: true,
                model: Course,
                as: 'courses',
                order: [
                    ['id', 'DESC'],
                ],
                include: [{
                    // all: true,
                    model: Facility,
                    as: 'facilityInformation',
                }, {
                    // all: true,
                    model: CourseEnrollment,
                    as: 'enrolmentRecord',
                    include: [{
                        // all: true,
                        model: Person,
                        as: 'StudentEnrolledRecord',
                    }],
                }]
            }, {
                // all: true,
                model: Category,
                as: 'categoryInformation',
            }, {
                // all: true,
                model: SubCategory,
                as: 'subCategoryInformation',
            }, {
                // all: true,
                model: Currency,
                as: 'currencyInformation',
            }, {
                model: Studio,
                as: 'studioInformation',
                // all: true
            }],
        }],
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}
/**
 *
 * Get Course details based on the Course Id
 *
 */
exports.GetSpecificCourse = function(req, res) {
    if (req.validationErrors(req.checkBody("course_id").notEmpty()) != false) return res.send(generalService.ChangeValidationErrorObject(228, 'course_id'));
    var orderBy = 'ASC';
    if (req.body.order_by != undefined) {
        orderBy = req.body.order_by
        if (orderBy != 'ASC' || orderBy != 'DESC') {
            orderBy = 'ASC'
        }
    }
    Course.findAll({
        where: {
            id: req.body.course_id
        },
        order: [
            ['createdAt', orderBy],
        ],
        include: [{
            model: CourseTemplate,
            as: 'courseTemplateInformation',
            include: [{
                model: Category,
                as: 'categoryInformation',
            }, {
                model: SubCategory,
                as: 'subCategoryInformation',
            }, {
                model: Currency,
                as: 'currencyInformation',
            }],
        }, {
            model: Facility,
            as: 'facilityInformation',
            include: [{
                model: StudioCenter,
                as: 'studioCenterRecord',
                include: [{
                    model: Address,
                    as: 'addressInformation',
                }]
            }]
        }, {
            model: Studio,
            as: 'studioInformation',
            include: [{
                model: StudioCenter,
                as: 'studioCenterRecord',
                include: [{
                    model: Address,
                    as: 'addressInformation',
                }]
            }]
        }, {
            model: Class,
            as: 'classRecord'
        }],
    }).then(sql1Result => {
        if (sql1Result.length > 0) {
            return res.json({
                "status": 1,
                "message": 'success',
                "code": 200,
                "record": sql1Result
            })
        } else {
            return res.json({
                "status": 0,
                "code": 223
            })
        }
    }).catch(function(err1) {
        if (err1) return res.json(err1)
    })
}