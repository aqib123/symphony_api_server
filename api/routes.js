var LoginController = require('./controllers/LoginController');
var StudentController = require('./controllers/StudentController');
var CourseController = require('./controllers/CourseController');
var StudioController = require('./controllers/StudioController');
var StudioCenterController = require('./controllers/StudioCenterController');
var FacilitiesController = require('./controllers/FacilitiesController');
var PaymentController = require('./controllers/PaymentController');
const upload = require('./services/upload_files');
const multer = require('multer');
// const logoUpload = upload.single('logo');
// const bannerUpload = upload.single('banner');
// Routes
module.exports = function(app) {
    // Main Routes
    /*=============================================================
    =            Section for User                                 =
    =============================================================*/
    // app.route('/send/code').post(LoginController.SendOTPCode);
    // app.route('/verify/number').post(LoginController.VarifyOtpCode);
    app.route('/registration').post(LoginController.Registration);
    app.route('/login').post(LoginController.Login);
    app.route('/verify/jwt').get(LoginController.loginRequired, LoginController.verifyToken);
    app.route('/check/mobile/number').post(LoginController.CheckMobileNumber);
    app.route('/get/user/info').post(LoginController.loginRequired, LoginController.GetUserDetails);
    app.route('/logout').post(LoginController.loginRequired, LoginController.Logout);
    app.route('/change/password').post(LoginController.loginRequired, LoginController.ChangePassword);
    app.route('/update/profile').post(LoginController.loginRequired, LoginController.UpdateProfile);
    app.route('/reset/password').post(LoginController.ResetPassword);
    app.route('/user/register/studio').post(LoginController.loginRequired, LoginController.CheckUserRegisterForStudio);
    app.route('/test/login').post(LoginController.CheckTestingLogin);
    /*=====  End of Section for User Controller  ======*/
    /*===========================================
    =            Section for Student            =
    ===========================================*/
    app.route('/student/create').post(LoginController.loginRequired, StudentController.Create);
    app.route('/student/update').post(LoginController.loginRequired, StudentController.Update);
    app.route('/student/delete').post(LoginController.loginRequired, StudentController.Delete);
    app.route('/student/record').post(LoginController.loginRequired, StudentController.StudentRecord);
    /*=====  End of Section for Student  ======*/
    /*=============================================
    =            Courses comment block            =
    =============================================*/
    app.route('/courses/enroll').post(LoginController.loginRequired, CourseController.EnrollCourse);
    app.route('/courses/student/enrolled').post(LoginController.loginRequired, CourseController.GetAllCoursesOfStudents);
    app.route('/courses/record').post(LoginController.loginRequired, CourseController.GetAllCoursesOfStudents);
    app.route('/get/courses/all').post(LoginController.loginRequired, CourseController.GetAllCourses);
    app.route('/get/courses/specific').post(CourseController.GetSpecificCourse);
    // app.route('/courses/all').post(LoginController.loginRequired, CourseController.GetAllCourses);
    /*=====  End of Courses comment block  ======*/
    /*=====================================================
    =            Section for STudio Controller            =
    =====================================================*/
    /**
     *
     * Course Template CRUD
     *
     */
    app.route('/studio/course/template/create').post(LoginController.loginRequired, StudioController.CreateCourseTemplate);
    app.route('/studio/course/template/update').post(LoginController.loginRequired, StudioController.UpdateCourseTemplate);
    app.route('/studio/course/template/delete').post(LoginController.loginRequired, StudioController.DeleteCourseTemplate);
    app.route('/studio/course/template/record').post(LoginController.loginRequired, StudioController.CourseTemplateRecord);
    /**
     *
     * Course CRUD
     *
     */
    app.route('/studio/course/create').post(LoginController.loginRequired, StudioController.CreateCourse);
    app.route('/studio/course/update').post(LoginController.loginRequired, StudioController.UpdateCourse);
    app.route('/studio/course/delete').post(LoginController.loginRequired, StudioController.DeleteCourse);
    app.route('/studio/course/record').post(LoginController.loginRequired, StudioController.CourseRecord);
    /**
     *
     * Categories CRUD
     *
     */
    app.route('/studio/category/create').post(LoginController.loginRequired, StudioController.CreateCategory);
    app.route('/studio/category/update').post(LoginController.loginRequired, StudioController.UpdateCategory);
    app.route('/studio/category/delete').post(LoginController.loginRequired, StudioController.DeleteCategory);
    app.route('/studio/category/record').post(LoginController.loginRequired, StudioController.CategoryRecord);
    /**
     *
     * Sub Categories CRUD
     *
     */
    app.route('/studio/subcategory/create').post(LoginController.loginRequired, StudioController.CreateSubCategory);
    app.route('/studio/subcategory/update').post(LoginController.loginRequired, StudioController.UpdateSubCategory);
    app.route('/studio/subcategory/delete').post(LoginController.loginRequired, StudioController.DeleteSubCategory);
    app.route('/studio/subcategory/record').post(LoginController.loginRequired, StudioController.SubCategoryRecord);
    /**
     *
     * Currencies CRUD
     *
     */
    app.route('/currency/create').post(LoginController.loginRequired, StudioController.CreateCurrency);
    app.route('/currency/update').post(LoginController.loginRequired, StudioController.UpdateCurrency);
    app.route('/currency/delete').post(LoginController.loginRequired, StudioController.DeleteCurrency);
    app.route('/currency/record').post(LoginController.loginRequired, StudioController.CurrencyRecord);
    /**
     *
     * Get Studio API's
     *
     */
    app.route('/get/studios/all').get(StudioController.GetAllStudioRecord);
    app.route('/get/tutors/all').post(LoginController.loginRequired, StudioController.GetAllTutors);
    app.route('/get/studio/centers/all').post(LoginController.loginRequired, StudioController.GetAllStudioCenters);
    app.route('/test/token').post(LoginController.loginRequired, StudioController.testToken);
    /**
     *
     * Studio Center CRUD
     *
     */
    app.route('/studio/center/create').post(LoginController.loginRequired, StudioCenterController.Create);
    app.route('/studio/center/update').post(LoginController.loginRequired, StudioCenterController.Update);
    app.route('/studio/center/delete').post(LoginController.loginRequired, StudioCenterController.Delete);
    app.route('/studio/center/record').post(LoginController.loginRequired, StudioCenterController.StudioCenterRecord);
    /**
     *
     * Facilities CRUD
     *
     */
    app.route('/facilities/create').post(LoginController.loginRequired, FacilitiesController.Create);
    app.route('/facilities/update').post(LoginController.loginRequired, FacilitiesController.Update);
    app.route('/facilities/delete').post(LoginController.loginRequired, FacilitiesController.Delete);
    app.route('/facilities/record').post(LoginController.loginRequired, FacilitiesController.FacilityRecord);
    /**
     *
     * Studio Info API's
     *
     */
    app.route('/studio/information').post(StudioController.GetStudioInformation);
    /**
     *
     * Switch Parent Account to studio Account.
     *
     */
    app.route('/studio/register').post(LoginController.loginRequired, StudioController.RegisterAsStudio);
    app.route('/studio/update/profile').post(LoginController.loginRequired, StudioController.UpdateStudioProfile);
    app.route('/studio/files/upload').post(upload.fields([{
        name: 'logo'
    }, {
        name: 'banner'
    }]), StudioController.UploadStudioFiles);
    /**
     *
     * Payment Transaction Page
     *
     */
    // app.route('/payment/transaction/page').post(LoginController.loginRequired, PaymentController.PaymentPage)
    /*=====  End of Section for STudio Controller  ======*/
};